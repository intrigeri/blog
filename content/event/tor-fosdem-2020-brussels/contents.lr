title: FOSDEM 2020 (Brussels)
---
author: steph
---
start_date: 2020-02-01
---
body:


Tor Meetup
==========

* **Track**: [BOFs (Track A - in J.1.106)](https://fosdem.org/2020/schedule/track/bofs_track_a_in_j1106/)
* **Room**: [J.1.106](https://fosdem.org/2020/schedule/room/j1106/)
* **Day**: [Saturday](https://fosdem.org/2020/schedule/day/saturday/)
* **Start**: [14:00](https://fosdem.org/2020/schedule/day/saturday/#1400 "2020-02-01T14:00:00+01:00")
* **End**: [15:00](https://fosdem.org/2020/schedule/day/saturday/#1500 "2020-02-01T15:00:00+01:00")

State of the Onion
==================

The Road to Mainstream Adoption and Improved Censorship Circumvention
----------------------------------------------------------------------

* **Track**: [Internet](https://fosdem.org/2020/schedule/track/internet/)
* **Room**: [Janson](https://fosdem.org/2020/schedule/room/janson/)
* **Day**: [Saturday](https://fosdem.org/2020/schedule/day/saturday/)
* **Start**: [17:00](https://fosdem.org/2020/schedule/day/saturday/#1700 "2020-02-01T17:00:00+01:00")
* **End**: [17:50](https://fosdem.org/2020/schedule/day/saturday/#1750 "2020-02-01T17:50:00+01:00")

The Tor Project is building usable free software to fight surveillance and censorship across the globe. In this talk we'll give an update on what we got up to during 2019, what happened in the wider Tor ecosystem, and what lies ahead of us.

During the past year the Tor Project has been working hard on improving the software, building and training communities around the world as well as creating an anti-censorship team and roadmap that can push forward technologies to circumvent censorship.

This talk will cover major milestones we achieved and will give an outline about what is lying ahead. In particular, we'll talk about our work to scale the network so it can cope with increased demand as we move forward with our plans for mainstream adoption of Tor Browser and the Tor network.

We will also share updates about our anti-censorship efforts, a year on from the formation of a dedicated Anti-Censorship team, and their work on next generation pluggable transports. Moreover, we'll explain our defense against website traffic fingerprinting attacks and plans for improving onion services and making them more usable (DDoS resistance, better user interfaces for authentication and dealing with errors).

Finally, we'll shed some light onefforts to get Tor support directly embedded into other browsers, like Firefox and Brave, and educating users both by reorganizing the content on our website, creating dedicated community and developer portals and extensive trainings throughout the world.

### Speakers

Pili

Website:
[Website ](https://fosdem.org/2020/schedule/event/tor/)
