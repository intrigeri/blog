title: New Release: Tor Browser 10.0.7
---
pub_date: 2020-12-15
---
author: sysrqb
---
tags:

tor browser
tbb
tbb-10.0
---
categories: applications
---
summary: Tor Browser 10.0.7 is now available from the Tor Browser download page and also from our distribution directory.
---
_html_body:

<p><strong>Update</strong> 2020-12-22 20:00 UTC: Tor Browser 10.0.7 is approved now on Google Play and it is rolling out.</p>
<p>Tor Browser 10.0.7 is now available from the <a href="https://www.torproject.org/download/">Tor Browser download page</a> and also from our <a href="https://www.torproject.org/dist/torbrowser/10.0.7/">distribution directory</a>.</p>
<p>This release updates Firefox for desktops to 78.6.0esr and Firefox for Android to 84.1.0. This release includes important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-55/">security updates</a> to Firefox for Desktop, and similar important <a href="https://www.mozilla.org/en-US/security/advisories/mfsa2020-54/">security updates</a> to Firefox for Android.</p>
<p><strong>Note:</strong> This update is not available on Google Play at this time because the update was rejected during the review process. We are appealing the rejection and working with Google so this update is available as soon as possible.</p>
<p>The full changelog since <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.0-desktop">Desktop</a> and <a href="https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-10.0-android">Android </a>Tor Browser 10.0.6 is:</p>
<ul>
<li>All Platforms
<ul>
<li>Update HTTPS Everywhere to 2020.11.17</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40166">Bug 40166</a>: Disable security.certerrors.mitm.auto_enable_enterprise_roots</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40176">Bug 40176</a>: Update openssl to 1.1.1i</li>
</ul>
</li>
<li>Windows + OS X + Linux
<ul>
<li>Update Firefox to 78.6.0esr</li>
</ul>
</li>
<li>Android
<ul>
<li>Update Firefox to 84.1.0</li>
<li>Update NoScript to 11.1.6</li>
</ul>
</li>
<li>Linux
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser/40226">Bug 40226</a>: Crash on Fedora Workstation Rawhide GNOME</li>
</ul>
</li>
<li>Build System
<ul>
<li>All Platforms
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40139">Bug 40139</a>: Pick up rbm commit for bug 40008</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40161">Bug 40161</a>: Update Go compiler to 1.14.13</li>
</ul>
</li>
<li>Android
<ul>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40128">Bug 40128</a>: Allow updating Fenix allowed_addons.json</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40140">Bug 40140</a>: Create own Gradle project</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40155">Bug 40155</a>: Update toolchain for Fenix 84</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40156">Bug 40156</a>: Update Fenix and dependencies to 84.0.0-beta2</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40163">Bug 40163</a>: Avoid checking hash of .pom files</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40171">Bug 40171</a>: Include all uniffi-rs artifacts into application-services</li>
<li><a href="https://bugs.torproject.org/tpo/applications/tor-browser-build/40184">Bug 40184</a>: Update Fenix and deps to 84.1.0</li>
</ul>
</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-290747"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290747" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2020</p>
    </div>
    <a href="#comment-290747">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290747" class="permalink" rel="bookmark">Please set the default of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please set the default of <span class="geshifilter"><code class="php geshifilter-php">media<span style="color: #339933;">.</span>videocontrols<span style="color: #339933;">.</span>picture<span style="color: #339933;">-</span>in<span style="color: #339933;">-</span>picture<span style="color: #339933;">.</span>video<span style="color: #339933;">-</span>toggle<span style="color: #339933;">.</span>enabled</code></span> to <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #009900; font-weight: bold;">false</span></code></span> to hide the button. As long as the related preference <span class="geshifilter"><code class="php geshifilter-php">media<span style="color: #339933;">.</span>videocontrols<span style="color: #339933;">.</span>picture<span style="color: #339933;">-</span>in<span style="color: #339933;">-</span>picture<span style="color: #339933;">.</span>enabled</code></span> is <span class="geshifilter"><code class="php geshifilter-php"><span style="color: #009900; font-weight: bold;">false</span></code></span>, the toggle button that pops up is useless and annoying to see.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290757"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290757" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 16, 2020</p>
    </div>
    <a href="#comment-290757">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290757" class="permalink" rel="bookmark">[OT but important]
@ Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>[OT but important]</p>
<p>@ Tor Project:</p>
<p>How does the SUNBURST backdoor in SolarWinds Orion network monitoring system possibly affect Tor infrastructure?  Can YaYa (Eff.org) detect SUNBURST?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290761"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290761" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Hmp (not verified)</span> said:</p>
      <p class="date-time">December 16, 2020</p>
    </div>
    <a href="#comment-290761">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290761" class="permalink" rel="bookmark">With the way several U.S…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>With the way several U.S. departments were attacked after they allowed SolarWinds to update, is the update feature for Tor safe to use, or has it been audited for exploits?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290765"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290765" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 17, 2020</p>
    </div>
    <a href="#comment-290765">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290765" class="permalink" rel="bookmark">TBB 10.0.7 works fine on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TBB 10.0.7 works fine on Windows-7 32-bit, THNX</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290775"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290775" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>J (not verified)</span> said:</p>
      <p class="date-time">December 18, 2020</p>
    </div>
    <a href="#comment-290775">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290775" class="permalink" rel="bookmark">Thank you!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290825"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290825" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 26, 2020</p>
    </div>
    <a href="#comment-290825">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290825" class="permalink" rel="bookmark">Any timeline on cache…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any timeline on cache partitioning?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-290832"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-290832" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 28, 2020</p>
    </div>
    <a href="#comment-290832">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-290832" class="permalink" rel="bookmark">I can&#039;t upload files on…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I can't upload files on Android? File selection prompt comes up and you can select a file but nothing gets uploaded.</p>
</div>
  </div>
</article>
<!-- Comment END -->
