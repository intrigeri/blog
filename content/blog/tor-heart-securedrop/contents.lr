title: Tor at the Heart: SecureDrop
---
pub_date: 2016-12-06
---
author: ssteele
---
tags:

heart of Internet freedom
SecureDrop
---
_html_body:

<p><i>During the month of December, we're highlighting other organizations and projects that rely on Tor, build on Tor, or are accomplishing their missions better because Tor exists. Check out our blog each day to learn about our fellow travelers. And please support the Tor Project! We're at the heart of Internet freedom.</i><br />
<a href="https://torproject.org/donate/donate-blog7" rel="nofollow">Donate today</a>!</p>

<p><b>SecureDrop</b></p>

<p><a href="https://securedrop.org/" rel="nofollow">SecureDrop</a> is an open-source whistleblower submission system that media organizations can install to accept documents from anonymous sources. It was originally coded by the late <a href="https://en.wikipedia.org/wiki/Aaron_Swartz" rel="nofollow">Aaron Swartz</a>, with assistance from <a href="https://www.wired.com/author/kevin_poulsen/" rel="nofollow">Wired editor Kevin Poulsen</a> and James Dolan. The project was previously called DeadDrop. <a href="https://freedom.press/" rel="nofollow">Freedom of the Press Foundation</a> took over management of the project in October 2013. </p>

<p>SecureDrop works by using two physical servers: a public-facing server that stores messages and documents, and a second server that performs security monitoring of the first. The code on the public-facing server is a Python web application that accepts messages and documents from the web and GPG-encrypts them for secure storage. This site is only made available as a Tor Hidden Service, which requires sources to use Tor, thus hiding their identity from both the SecureDrop server and many types of network attackers. Essentially, it’s a more secure alternative to the "contact us" form found on a typical news site. Every source who visits the site is given a unique "codename." The codename lets the source establish a relationship with the news organization without revealing his/her real identity or resorting to e-mail. They can enter the code name on a future visit to read any messages sent back from the journalist, or to submit additional documents and messages under the same persistent, but pseudonymous, identifier. The source is known by a different and unrelated code name on the journalist’s side. All of the source’s submissions, and replies to the source from journalists, are grouped together into a collection. Every time there’s a new submission by a source, their collection is bumped to the top of the submission queue. </p>

<p>The SecureDrop application does not record your IP address, information about your browser, computer, or operating system. Furthermore, the SecureDrop pages do not embed third-party content or deliver persistent cookies to your browser. The server will only store the date and time of the newest message sent from each source. Once you send a new message, the time and date of your previous message is automatically deleted. Journalists are also encouraged to regularly delete all information from the SecureDrop server and store anything they would like saved in offline storage to minimize risk.</p>

<p>Over three dozen media organizations are currently using SecureDrop, including:</p>

<ul type="disc">
<li><a href="https://berlinleaks.org" rel="nofollow">BerlinLeaks</a></li>
<li><a href="https://securedrop.cbc.ca" rel="nofollow">CBC</a></li>
<li><a href="https://www.cpj.org/campaigns/assistance/how-to-get-help.php" rel="nofollow">CPJ</a></li>
<li><a href="https://espenandersen.no/contact" rel="nofollow">Espen Andersen</a></li>
<li><a href="https://exposefacts.org" rel="nofollow">ExposeFacts</a></li>
<li><a href="https://gawkermediagroup.com/securedrop" rel="nofollow">Gawker Media</a></li>
<li><a href="https://sec.theglobeandmail.com/securedrop" rel="nofollow">Toronto Globe and Mail</a></li>
<li><a href="https://www.safesource.org.nz" rel="nofollow">Greenpeace New Zealand</a></li>
<li><a href="https://securedrop.theguardian.com" rel="nofollow">The Guardian</a></li>
<li><a href="https://firstlook.org/theintercept/securedrop" rel="nofollow">The Intercept</a></li>
<li><a href="https://lucyparsonslabs.com/securedrop" rel="nofollow">Lucy Parsons Labs</a></li>
<li><a href="https://neos.eu/leaks/" rel="nofollow">NEOSleaks</a></li>
<li><a href="https://projects.newyorker.com/strongbox" rel="nofollow">The New Yorker</a></li>
<li><a href="https://www.nrk.no/varsle/" rel="nofollow">NRKbeta</a></li>
<li><a href="https://securedrop.pogo.org" rel="nofollow">Project On Gov't Oversight (POGO) </a></li>
<li><a href="https://securedrop.propublica.org" rel="nofollow">ProPublica</a></li>
<li><a href="https://securedrop.radio24syv.dk" rel="nofollow">Radio24syv</a></li>
<li><a href="https://sourceanonyme.radio-canada.ca" rel="nofollow">Radio-Canada</a></li>
<li><a href="https://bayleaks.org" rel="nofollow">BayLeaks</a></li>
<li><a href="https://tcfmailvault.info" rel="nofollow">Barton Gellman</a></li>
<li><a href="https://www.washingtonpost.com/securedrop" rel="nofollow">The Washington Post</a></li>
<li><a href="https://news.vice.com/securedrop" rel="nofollow">VICE Media</a>    </li>
<li><a href="https://freedom.press/about/tech/kevin-poulsen" rel="nofollow">Wired's Kevin Poulsen</a></li>
</ul>

---
_comments:

<a id="comment-224245"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224245" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2016</p>
    </div>
    <a href="#comment-224245">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224245" class="permalink" rel="bookmark">The other advantage of</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>The other advantage of running a hidden service for SecureDrop is that if a potential whistelblower messes up the configuration (for whatever reason), they wont be able to connect to SecureDrop, since it only works with Tor. That's also how Wikileaks operate.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224297"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224297" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 07, 2016</p>
    </div>
    <a href="#comment-224297">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224297" class="permalink" rel="bookmark">Thanks for this! I remember</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks for this! I remember hearing about the old DeadDrop site, but I never knew what it was for or that it still existed. I'm glad the developers and site operators are here to provide a secure alternative to the terribly insecure email protocol.</p>
<p>If this has been around since Aaron Swartz, and it's used by the Guardian, then (if I have my dates and facts straight), why didn't Snowden use it and avoid the LavaBit fiasco?</p>
<p>Also,<br />
&gt; The SecureDrop application does not record your IP address...<br />
Application? Is this referring to the .onion site accessed via Tor Browser, or does the end-user run the software on his or her computer for some reason?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224360"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224360" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
    <a href="#comment-224360">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224360" class="permalink" rel="bookmark">GitHub changed their site to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>GitHub changed their site to require JavaScript to download source a while back. Goes against security guidelines for those downloading SecureDrop over Tor.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224427"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224427" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 08, 2016</p>
    </div>
    <a href="#comment-224427">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224427" class="permalink" rel="bookmark">Thanks to the Tor Project</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks to the Tor Project for helping build the necessary tools to protect whistleblowers, the world is a darker place without you!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224595"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224595" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 09, 2016</p>
    </div>
    <a href="#comment-224595">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224595" class="permalink" rel="bookmark">They &quot;Traffic shape&quot; traffic</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>They "Traffic shape" traffic by being in control of websites to require certain aspects to load, it is similar as to them banning Tor over Cloud Hosting.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-224861"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224861" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 12, 2016</p>
    </div>
    <a href="#comment-224861">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224861" class="permalink" rel="bookmark">&gt; It was originally coded by</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; It was originally coded by the late Aaron Swartz, with assistance from Wired editor Kevin Poulsen and James Dolan.</p>
<p>Tragically, we cannot thank Aaron Swartz, but I want to thank  Kevin Poulsen and James Dolan for their work on this invaluable journalism tool, and I hope its development will continue, ideally informed by an indepedent security audit.</p>
<p>"M.E."</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-225012"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-225012" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 13, 2016</p>
    </div>
    <a href="#comment-225012">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-225012" class="permalink" rel="bookmark">Where is boom berg Business</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Where is boom berg Business and fox business / the fortune / Forbes </p>
<p>news? Financial times new? Reuters stock exchange new   are they </p>
<p>broadcast those news where Fake and unreliable?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-230511"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-230511" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 11, 2017</p>
    </div>
    <a href="#comment-230511">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-230511" class="permalink" rel="bookmark">Is this the Propublica</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is this the Propublica Secure Drop site?</p>
<p><a href="http://pubdrop4dw6rk3aq.onion/" rel="nofollow">http://pubdrop4dw6rk3aq.onion/</a></p>
<p>Why is the key not signed by anyone affiliated with Propublica?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-231480"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-231480" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">January 18, 2017</p>
    </div>
    <a href="#comment-231480">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-231480" class="permalink" rel="bookmark">How to verify that these are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How to verify that these are genuine?</p>
<p>Intercept-Secure-Drop<br />
<a href="http://y6xjgkgwj47us5ca.onion/" rel="nofollow">http://y6xjgkgwj47us5ca.onion/</a></p>
<p>Propublica Secure Drop site<br />
<a href="http://pubdrop4dw6rk3aq.onion/" rel="nofollow">http://pubdrop4dw6rk3aq.onion/</a></p>
<p>I see certificates which appear to be good, but I worry.</p>
<p>How long should one expect to wait for a reply?</p>
</div>
  </div>
</article>
<!-- Comment END -->
