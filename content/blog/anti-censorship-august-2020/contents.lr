title: Anti-censorship team report: August 2020
---
pub_date: 2020-09-04
---
author: phw
---
tags:

anti-censorship
monthly status
---
categories:

circumvention
reports
---
_html_body:

<p>Tor's anti-censorship team writes monthly reports to keep the world updated on its progress. This blog post summarizes the anti-censorship work we got done in August 2020. Let us know if you have any questions or feedback!</p>
<h2>Snowflake</h2>
<ul>
<li>
<p>We set up an anonymous bug-reporting pad for the team. Take a look at our reporting instructions:<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/wikis/home#reporting-bugs">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a><br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake-webext/-/issues/14">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
<li>
<p>NAT type matching in Snowflake browser proxies. Released version 0.4.1 of the browser extension.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/34129">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
<li>
<p>Perform a test for symmetric NATs on startup.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake-webext/-/issues/13">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
<li>
<p>Implemented a fix so Snowflake stops using the client's network when Tor isn't making reqests.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/21314">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
<li>
<p>Hashik Donthineni, our GSoC 2020 student, successfully finished his Android proxy app project! It needs more UI improvements and testing before being ready for use.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake-mobile/-/wikis/home">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
<li>
<p>Made progress on allowing clients to split traffic across multiple proxies.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/25723">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
<li>
<p>Updated Snowflake stats to include counts of restricted, unrestricted, and unknown proxies.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake/-/issues/40008">https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/…</a></p>
</li>
</ul>
<h2>BridgeDB</h2>
<ul>
<li>
<p>Inspected several months worth of BridgeDB's HTTPS distributor logs and distilled a few insights.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgedb/-/issues/31871">https://gitlab.torproject.org/tpo/anti-censorship/bridgedb/-/issues/318…</a></p>
</li>
<li>
<p>Took a closer look at bot scraping attempts.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgedb/-/issues/32117">https://gitlab.torproject.org/tpo/anti-censorship/bridgedb/-/issues/321…</a></p>
</li>
</ul>
<h2>Rdsys</h2>
<ul>
<li>
<p>Decided to call our BridgeDB redesign rdsys, which is short for "resource distribution system." Do you have a better name? If so, let us know!</p>
</li>
<li>
<p>Created a GitLab repository for rdsys. The code is a messy construction site and will continue to be in the foreseeable future.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys">https://gitlab.torproject.org/tpo/anti-censorship/rdsys</a></p>
</li>
<li>
<p>Improved the way rdsys talks to bridgestrap to verify that bridges are working.</p>
</li>
<li>
<p>Built an HTTP streaming interface between rdsys's backend and its distributors. This interface allows the backend to stream resource updates to the distributors without delay. The goal is that resources (i.e. bridges) are distributed to users immediately after they are added to the system.</p>
</li>
</ul>
<h2>Salmon</h2>
<ul>
<li>Implemented a crude prototype of the Salmon bridge distribution mechanism. Brainstormed a handful of privacy-preserving features that would improve the original design.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/1">https://gitlab.torproject.org/tpo/anti-censorship/rdsys/-/issues/1</a></li>
</ul>
<h2>Bridgestrap</h2>
<ul>
<li>
<p>Improved the service's shutdown procedure, caching mechanism, its documentation, and its HTTP API. Added a command line flag to print bridgestrap's cache.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap">https://gitlab.torproject.org/tpo/anti-censorship/bridgestrap</a></p>
</li>
<li>
<p>Added a token bucket mechanism to limit the number of requests that the service accepts.</p>
</li>
</ul>
<h2>Emma</h2>
<ul>
<li>Updated the resources that emma tries to connect to (replaced a guard relay and removed the default bridge "frosty"). Changed trac links to GitLab links.<br />
    <a href="https://gitlab.torproject.org/tpo/anti-censorship/emma">https://gitlab.torproject.org/tpo/anti-censorship/emma</a></li>
</ul>
<h2>Miscellaneous</h2>
<ul>
<li>Retired the default bridge "frosty" because the colocation site where the bridge was running will be shut down.<br />
    <a href="https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40066">https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issu…</a></li>
</ul>
<h2>Outreach</h2>
<ul>
<li>
<p>Published a blog post on our challenges, priorities, and progress.<br />
    <a href="https://blog.torproject.org/anti-censorship-challenges-priorities-progress">https://blog.torproject.org/anti-censorship-challenges-priorities-progr…</a></p>
</li>
<li>
<p>Published our July 2020 report as blog post.<br />
    <a href="https://blog.torproject.org/anti-censorship-july-2020">https://blog.torproject.org/anti-censorship-july-2020</a></p>
</li>
<li>
<p>David presented his Turbo Tunnel paper at FOCI'2020. His talk is available online:<br />
    <a href="https://www.usenix.org/conference/foci20/presentation/fifield">https://www.usenix.org/conference/foci20/presentation/fifield</a></p>
</li>
<li>
<p>Roger participated in a FOCI'2020 panel on "Internet Freedom in the Domestic Arena."</p>
</li>
<li>
<p>Cecylia was a panelist at Tor's latest PrivChat iteration, called "the Good, the Bad, and the Ugly of Censorship Circumvention." The recording is available online:<br />
    <a href="https://www.youtube.com/watch?v=aOOChyMCZH4">https://www.youtube.com/watch?v=aOOChyMCZH4</a></p>
</li>
</ul>

---
_comments:

<a id="comment-289384"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289384" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 04, 2020</p>
    </div>
    <a href="#comment-289384">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289384" class="permalink" rel="bookmark">Thanks again! :)
~ one of…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanks again! :)</p>
<p>~ one of your avid snowflake users</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289387"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289387" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anon RelayOp (not verified)</span> said:</p>
      <p class="date-time">September 04, 2020</p>
    </div>
    <a href="#comment-289387">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289387" class="permalink" rel="bookmark">Rdsys = ReadySys</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Rdsys = ReadySys</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289396"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289396" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 06, 2020</p>
    </div>
    <a href="#comment-289396">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289396" class="permalink" rel="bookmark">Any estimate on when…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Any estimate on when Snowflake will be integrated into the main Tor Browser?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289414"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289414" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">September 08, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289396" class="permalink" rel="bookmark">Any estimate on when…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289414">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289414" class="permalink" rel="bookmark">We&#039;re aiming for the Tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>We're aiming for the Tor Browser 10.5 release, which should be out in Winter/Spring.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289404"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289404" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 07, 2020</p>
    </div>
    <a href="#comment-289404">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289404" class="permalink" rel="bookmark">If you&#039;re not going to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you're not going to approve comments, at least turn the discussions off.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289415"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289415" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">September 08, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289404" class="permalink" rel="bookmark">If you&#039;re not going to…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289415">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289415" class="permalink" rel="bookmark">Sorry about that. I&#039;m trying…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Sorry about that. I'm trying to approve comments at least once a day but I don't always manage to.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289407"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289407" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Rose (not verified)</span> said:</p>
      <p class="date-time">September 07, 2020</p>
    </div>
    <a href="#comment-289407">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289407" class="permalink" rel="bookmark">Hi Tor Team,
There seems to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hi Tor Team,</p>
<p>There seems to be a problem with your site. After I tried to post your page kept refreshing itself. In order to read the page I needed to click the Stop button (alongside the Forward and Back buttons). There seems to be something stored in a cookie because the only way to prevent this is to reset Tor Browser. Using the Broomstick button.</p>
<p>It didn't look like my comment was successfully posted to your recent alpha release blog page. So I'll post here instead. This is what I wrote:</p>
<p>"Hi Tor Team,</p>
<p>It's good to see that the Bug (sic: bugfix rather) "Let JavaScript on safest setting handled by NoScript again" is added.</p>
<p>Can this be rolled out for immediate and emergency release on the non-Alpha release. For almost a month, I've not been about to keep in touch with various operators that happen to be on a site that also runs google js.</p>
<p>May I ask, where did the need to break Tor's safest setting come from? Who instigated this and why? This is important to know so that we can prevent future problems.</p>
<p>This is likely proving a traffic analyser's dream, because Tor have been able to divide users by those who have downgraded their security to allow all JS and those who refuse to downgrade. Not good."</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289425"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289425" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 09, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289407" class="permalink" rel="bookmark">Hi Tor Team,
There seems to…</a> by <span>Rose (not verified)</span></p>
    <a href="#comment-289425">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289425" class="permalink" rel="bookmark">&gt; After I tried to post your…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; After I tried to post your page kept refreshing itself.</p>
<p>Turn on Safer, Standard, or javascript. Here is the original ticket because I can't find it on Gitlab. <a href="https://trac.torproject.org/projects/tor/ticket/22530" rel="nofollow">https://trac.torproject.org/projects/tor/ticket/22530</a></p>
<p>&gt; where did the need to break Tor's safest setting come from? Who instigated this and why?</p>
<p>Javascript always has been disabled by default in the safest/highest security setting. As for why Javascript does not become enabled if you go into NoScript and tweak it yourself and you're in the Safest setting, read here: <a href="https://blog.torproject.org/new-release-tor-browser-907" rel="nofollow">https://blog.torproject.org/new-release-tor-browser-907</a></p>
<p>See also:<br />
<a href="https://tb-manual.torproject.org/plugins/" rel="nofollow">https://tb-manual.torproject.org/plugins/</a><br />
<a href="https://support.torproject.org/tbb/tbb-39/" rel="nofollow">https://support.torproject.org/tbb/tbb-39/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289417"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289417" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 08, 2020</p>
    </div>
    <a href="#comment-289417">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289417" class="permalink" rel="bookmark">9/9/20, 05:34:30.821 [NOTICE…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>9/9/20, 05:34:30.821 [NOTICE] Bootstrapped 10% (conn_done): Connected to a relay<br />
9/9/20, 05:37:04.818 [NOTICE] Bootstrapped 14% (handshake): Handshaking with a relay<br />
WTF?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289421"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289421" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">September 09, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289417" class="permalink" rel="bookmark">9/9/20, 05:34:30.821 [NOTICE…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289421">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289421" class="permalink" rel="bookmark">Is your client stuck at 14%?…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is your client stuck at 14%? What's the problem?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-289469"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289469" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 15, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289417" class="permalink" rel="bookmark">9/9/20, 05:34:30.821 [NOTICE…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-289469">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289469" class="permalink" rel="bookmark">If you only waited a few…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>If you only waited a few seconds you might not have given your Tor client enough time to set things up.</p>
<p>The Tor client is the basic Tor software, which runs "under" Tor Browser, which is based on Firefix.  The Tor client software comes bundled with Tor Browser.  It is set up automatically when you unpack the compressed file you downloaded from Torproject.org and unpacked somewhere in your computer.</p>
<p>To connect to the Tor network, your Tor client needs to contact the Tor Directory authorities, then download current information about the Tor network, then to start building Tor circuits.  Once you have enough Tor circuits, Tor Browser tells you that you are reading to browse the Web.</p>
<p>FWIW, "handshake" refers to using a public/private keypair to establish an encrypted connection with a Tor node (or,  I think, with a Directory Authority).  It looks like your client was trying to build circuits when... you gave up too soon?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-289419"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289419" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Armorx9 (not verified)</span> said:</p>
      <p class="date-time">September 09, 2020</p>
    </div>
    <a href="#comment-289419">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289419" class="permalink" rel="bookmark">how to get  tor links</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>how to get  tor links</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-289422"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-289422" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phw
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">Philipp Winter</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted"><a class="tor" title="View user profile." href="/user/46">Philipp Winter</a> said:</p>
      <p class="date-time">September 09, 2020</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-289419" class="permalink" rel="bookmark">how to get  tor links</a> by <span>Armorx9 (not verified)</span></p>
    <a href="#comment-289422">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-289422" class="permalink" rel="bookmark">What do you mean by &quot;tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>What do you mean by "tor links"? You can download Tor Browser here:<br />
<a href="https://www.torproject.org/download/" rel="nofollow">https://www.torproject.org/download/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
