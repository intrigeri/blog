title: Tor + Outreachy: Internships for Underrepresented People in Tech
---
pub_date: 2018-02-21
---
author: tommy
---
tags:

internship
internships
---
categories: internships
---
summary: The Tor Project has partnered with the Outreachy internship program, and we’ve got two internship slots for members of groups traditionally underrepresented in technology.
---
_html_body:

<p> </p>
<p>The Tor Project has partnered with the <a href="https://www.outreachy.org/">Outreachy</a> internship program, and we’ve got two internship slots for members of groups traditionally underrepresented in technology.</p>
<p>This is Tor’s <a href="https://medium.com/@parinishthayadav/my-foray-into-open-source-working-with-tor-as-an-outreachy-intern-8c93b954826b">second time</a> participating in Outreachy’s three-month internship program. This round runs from May to August, 2018, and applications are due on March 22.</p>
<h3><b>Who’s eligible for Outreachy?</b></h3>
<blockquote><p>Outreachy internships are open internationally to women (cis and trans), trans men, and genderqueer people. Internships are also open to residents and nationals of the United States of any gender who are Black/African American, Hispanic/Latin@, Native American/American Indian, Alaska Native, Native Hawaiian, or Pacific Islander.<br />
—<a href="https://www.outreachy.org/">Outreachy.org</a>.</p>
</blockquote>
<p>We’re committed to inclusion in all facets of Tor development, and having a diverse range of backgrounds, viewpoints, and experience helps us foster a healthy development community. Outreachy encourages underrepresented groups to learn new skills, meet new people in their field, and contribute to critical open source tools.</p>
<h3><b>Use your skills</b></h3>
<p>Here are the internship opportunities we’ve got planned for this Outreachy round. If one catches your eye, start your <a href="https://www.outreachy.org/apply/">application</a> now.</p>
<ul>
<li><b>User Advocate</b></li>
</ul>
<p>Tor is looking for a user advocate who would work with members of Tor’s development and community teams to help us understand where users struggle to use Tor. A secure web browser only works if it’s usable. </p>
<p>This intern will monitor our bug-reporting channels, IRC rooms, and social channels to get a sense of what new bugs appear in our software, and where users have problems connecting to and using Tor. Each month, we’d like the intern to report on the most common bugs, problems, questions, and other issues that users have.</p>
<ul>
<li><b>Documentation Editor</b></li>
</ul>
<p>For this task, the intern would help the community team to update, review, and format sections of the Tor documentation on how to add Tor’s privacy and anonymity protections to other apps. Much of this documentation is written by volunteers and is not regularly maintained. Tor is about to transition to a new support portal, and the intern would help make this content ready to be displayed on the new website.</p>
<p>To get started on this task, check out <a href="https://www.torproject.org/docs/documentation.html.en">Tor’s documentation pages</a>. We’d like the intern to focus on updating <a href="https://trac.torproject.org/projects/tor/wiki/Outreachy/2018doclist">these pages</a> in particular.</p>
<h3>Get in touch </h3>
<p>We look forward to seeing your applications. If you have any questions along the way, you can leave a comment here or get in touch with Tor’s Outreachy coordinator. Check our <a href="https://trac.torproject.org/projects/tor/wiki/Outreachy">Outreachy page</a> for contact details. There’ll also be a Twitter chat on Monday, February 26 at 4pm UTC / 11am EDT / 8am PDT, where Tor’s coordinator and our mentor for this round will be answering questions using the #OutreachyChat tag.</p>

---
_comments:

<a id="comment-274042"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274042" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Jackie (not verified)</span> said:</p>
      <p class="date-time">February 21, 2018</p>
    </div>
    <a href="#comment-274042">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274042" class="permalink" rel="bookmark">It&#039;s good to get more people…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It's good to get more people involved but unfortunate that the organization has chosen to discriminate in these ways. Why not allow anyone, regardless of DNA/nationality/etc., who has faced discrimination or reasonably considers themselves an outsider to this group? Or to go farther, anyone that hasn't worked much with technology and/or free software, and would like some help to get involved.</p>
<p>Every hero needs a mentor, every hero needs a helping hand... and even people who are in the "right" categories might not feel comfortable putting that on record.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274053"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274053" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  tommy
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">t0mmy</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">t0mmy said:</p>
      <p class="date-time">February 22, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274042" class="permalink" rel="bookmark">It&#039;s good to get more people…</a> by <span>Jackie (not verified)</span></p>
    <a href="#comment-274053">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274053" class="permalink" rel="bookmark">Everyone is welcome to…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Everyone is welcome to contribute to Tor no matter their gender, race, religion, or level of ability. We're particularly interested in encouraging people who, as you say, might not have much experience with technology and/or free software. That said, in this particular instance, the underrepresented groups outlined above often face significant systematic barriers to entering the tech/open source software, and so the Outreachy internship is aimed specifically at these groups.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274047"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274047" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 21, 2018</p>
    </div>
    <a href="#comment-274047">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274047" class="permalink" rel="bookmark">Wonder what 55 Savushkina…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Wonder what 55 Savushkina Street will say about this.</p>
<p>We need another good media stunt, like that time Mayor Davydov boasted that "Svetogorsk has no gays", whereupon gay citizens snapped campy selfies in front of a local landmark.  The Mayor was so embarrased he actually had the landmark demolished a few days later.  </p>
<p>I have a feeling he will soon be looking for another job--- but probably not with Tor Project.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274051"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274051" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 22, 2018</p>
    </div>
    <a href="#comment-274051">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274051" class="permalink" rel="bookmark">hello all,…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>hello all,</p>
<p>may i suggest an app please ?<br />
- how the users will know that tor has not been implemented with a backdoor (subpoema_you could NOT avoid it) in a near future (who knows ?) if you do not prepare now an app for checking it ?</p>
<p>* how to know that it is not yet the case ? pgp verifies the chain trust not the source/binary.</p>
<p>have a nice day.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274111"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274111" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">February 26, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274051" class="permalink" rel="bookmark">hello all,…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-274111">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274111" class="permalink" rel="bookmark">You can study our source…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You can study our source code and then reproduce the binaries we ship bit-for-bit (on macOS this is a bit tricky right now due to code signing and we work on that). So, there is no special app needed here.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-274067"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274067" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">February 23, 2018</p>
    </div>
    <a href="#comment-274067">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274067" class="permalink" rel="bookmark">Just wanted to add to what…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just wanted to add to what t0mmy said something which might be helpful to non-US persons:</p>
<p>The US has a large and enormously diverse population, and hosts a large number of often small and narrowly focused NGOs.  Some of these sponsor internship programs intended to further the specific goals of their organization.  In my experience, it is quite common for internships to be aimed at recruiting people from specified minority groups.  In that context, the applicant pool specified in the post is actually rather broad.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274201"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274201" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Stephie (not verified)</span> said:</p>
      <p class="date-time">March 04, 2018</p>
    </div>
    <a href="#comment-274201">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274201" class="permalink" rel="bookmark">Just asking for some…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just asking for some clarifications; Are two interns selected for the two projects on this Outreachy round? </p>
<p>Do I add documentation for the various projects on their respective wiki pages or is there a different place where the documentation should be written before writing it on the wiki?</p>
<p>Thanks</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-274275"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274275" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  tommy
  <div class="field field--name-field-display-name field--type-string field--label-visually_hidden">
    <div class="field--label sr-only">Display Name</div>
              <div class="field--item">t0mmy</div>
          </div>
</article>
    <div class="comment-header">
      <p class="comment__submitted">t0mmy said:</p>
      <p class="date-time">March 12, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-274201" class="permalink" rel="bookmark">Just asking for some…</a> by <span>Stephie (not verified)</span></p>
    <a href="#comment-274275">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274275" class="permalink" rel="bookmark">Hey Stephie --…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Hey Stephie --<br />
Currently, we're planning to just have one intern, who can choose between the two projects outlined above. You can edit the documentation on the wiki page itself, or you can email me at tommyc(@)torproject.org. Cheers!</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
