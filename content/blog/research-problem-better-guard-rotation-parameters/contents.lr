title: Research problem: better guard rotation parameters
---
pub_date: 2011-08-20
---
author: arma
---
tags:

research
performance
anonymity
anonymity features
guard relays
attackers
protecting users
entry guards
---
categories:

community
network
relays
research
---
_html_body:

<p>Tor's <a href="https://www.torproject.org/docs/faq#EntryGuards" rel="nofollow">entry guard design</a> protects users in a variety of ways.</p>

<p>First, they protect against the "<a href="http://freehaven.net/anonbib/#Wright:2004" rel="nofollow">predecessor attack</a>": if you choose new relays for each circuit, eventually an attacker who runs a few relays will be your first and last hop. With entry guards, the risk of end-to-end correlation for any given circuit is the same, but the cumulative risk for all your circuits over time is capped.</p>

<p>Second, they help to protect against the "<a href="http://freehaven.net/anonbib/#ccs07-doa" rel="nofollow">denial of service as denial of anonymity</a>" attack, where an attacker who runs quite a few relays fails any circuit that he's a part of and that he can't win against, forcing Alice (the user) to generate more circuits and thus increasing the overall chance that the attacker wins. Entry guards greatly reduce the risk, since Alice will never choose outside of a few nodes for her first hop.</p>

<p>Third, entry guards raise the startup cost to an adversary who runs relays in order to trace users. Without entry guards, the attacker can sign up some relays and immediately start having chances to observe Alice's circuits. With them, new adversarial relays won't have the Guard flag so won't be chosen as the first hop of any circuit; and even once they earn the Guard flag, users who have already chosen guards won't switch away from their current guards for quite a while.</p>

<p>But how long exactly? The first research question here examines vulnerability due to natural churn of entry guards. Consider an adversary who runs one entry guard with advertised capacity C. Alice maintains an ordered list of guards (chosen at random, weighted by advertised speed, from the whole list of possible guards). For each circuit, she chooses her first hop (uniformly at random) from the first G guards from her list that are currently running (appending a new guard to the list as needed, but going back to earlier guards if they reappear). How long until Alice adds the adversary's node to her guard list? You can use the uptime data from the <a href="https://metrics.torproject.org/data.html" rel="nofollow">directory archives</a>, either to build a model for guard churn or just to use the churn data directly. Assuming G=3, how do the results vary by C? What's the variance?</p>

<p>Research question two: consider intentional churn due to load balancing too. Alice actually discards each guard between 30 and 60 days (uniformly chosen) after she first picks it. This intentional turnover prevents long-running guards from accumulating more and more users and getting overloaded. Another way of looking at it is that it shifts load to new guards so we can make better use of them. How much does this additional churn factor impact your answer from step one above? Or asked a different way, what fraction of Alice's vulnerability to the adversary's entry guard comes from natural churn, and what fraction from the proactive expiration? How does the answer change for different expiry intervals (e.g. between 10 and 30 days, or between 60 and 90)?</p>

<p>Research question three: how do these answers change as we vary G? Right now we choose from among the first three guards, to reduce the variance of expected user performance -- if we always picked the first guard on the list, and some users picked a low-capacity or highly-congested relay, none of that user's circuits would perform well. That said, if choosing G=1 is a huge win for security, we should work on other ways to reduce variance.</p>

<p>Research question four: how would these answers change if we make the cutoffs for getting the Guard flag more liberal, and/or change how we choose what nodes become guards? After all, Tor's anonymity is based on the <a href="https://blog.torproject.org/blog/research-problem-measuring-safety-tor-network" rel="nofollow">diversity of entry and exit points</a>, and while it may be tough to get around exit relay scarcity, my theory is that our artificial entry point scarcity (because our requirements are overly strict) is needlessly hurting the anonymity Tor can offer.</p>

<p>Our current algorithm for guard selection has three requirements:<br />
1) The relay needs to have first appeared longer ago than 12.5% of the relays, or 8 days ago, whichever is shorter.<br />
2) The relay needs to advertise at least the median bandwidth in the network, or 250KB/s, whichever is smaller.<br />
3) The relay needs to have at least the median weighted-fractional-uptime of relays in the network, or 98% WFU, whichever is smaller. (For WFU, the clock starts ticking when we first hear about the relay; we track the percentage of that time the relay has been up, discounting values by 95% every 12 hours.)</p>

<p>Today's guard cutoffs in practice are "was first sighted at least 8 days ago, advertises 100KB/s of bandwidth, and has 98% WFU."</p>

<p>Consider two relays, A and B. Relay A first appeared 30 days ago, disappeared for a week, and has been consistently up since then. It has a WFU (after decay, give or take a fencepost) of 786460 seconds / 824195 = 95.4%, so it's not a guard. Relay B appeared two weeks ago and has been up since then. Its WFU is 658517 / 658517 = 100%, so it gets the Guard flag -- even though it has less uptime, and even less weighted uptime.  Based on the answers to the first three research questions, which relay would serve Alice best as a guard?</p>

<p>The big-picture tradeoff to explore is: what algorithm should we use to assign Guard flags such that a) we assign the flag to as many relays as possible, yet b) we minimize the chance that Alice will use the adversary's node as a guard?</p>

---
_comments:

<a id="comment-10554"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10554" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 20, 2011</p>
    </div>
    <a href="#comment-10554">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10554" class="permalink" rel="bookmark">All three requirements for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>All three requirements for entry guards are easy to fulfill for an organized adversary. What does prevent an adversary to throw 100 virtual servers in the pool with above median bandwidth and full uptime and wait one week? Is there a criteria not to select all entry guards from the same IP block?</p>
<p>The critical question is how long does Alice stay with advesaries entry guard she had the pity to select? How much time does advesary need to have a sure correlation? </p>
<p>(Brainstorm mode on) What about virtualizing the entry guards? Each circuit contains two entry guards as the first hop in the path. On the way back the middle hop randomly selects one of the two pre-selected entry guards of that circuit to send the packet back.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-10557"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10557" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 20, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-10554" class="permalink" rel="bookmark">All three requirements for</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-10557">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10557" class="permalink" rel="bookmark">&gt; All three requirements for</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>&gt; All three requirements for entry guards are easy to fulfill for an organized adversary. What does prevent an adversary to throw 100 virtual servers in the pool with above median bandwidth and full uptime and wait one week?</p>
<p>Nothing prevents it. We once imagined we could run a Tor network only using relays run by people we knew. That approach doesn't scale, especially when anonymity is a function of network diversity. Instead, we need to grow a larger network: the more relays we have, the less impact a new attacking relay can have.</p>
<p>That said, with high probability, none of these 100 guards will be used by Alice after a week. That point is the main focus of this blog post.</p>
<p>&gt; Is there a criteria not to select all entry guards from the same IP block?</p>
<p>Yes. No more than one guard in the same /16 network.</p>
<p>&gt; The critical question is how long does Alice stay with advesaries entry guard she had the pity to select?</p>
<p>For many weeks. Bad news for Alice in this case.</p>
<p>&gt; How much time does advesary need to have a sure correlation?</p>
<p>Open research question. My guess is "not much time".</p>
<p>&gt; What about virtualizing the entry guards? Each circuit contains two entry guards as the first hop in the path. On the way back the middle hop randomly selects one of the two pre-selected entry guards of that circuit to send the packet back.</p>
<p>Maybe? I worry that two increases the attack surface over one, without much clear gain. See the discussion under <a href="https://blog.torproject.org/blog/one-cell-enough" rel="nofollow">https://blog.torproject.org/blog/one-cell-enough</a> as well.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-10905"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10905" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 26, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to arma</p>
    <a href="#comment-10905">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10905" class="permalink" rel="bookmark">first excuse me because my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>first excuse me because my english is not good.<br />
this program is a  nice job BUT really its not Secure for people in some countries that government forbid useing this kind of programs , the reason is this : its very easy for the Internet providers to download this program run that program and check the ip addresses that this program will use , if they do this 100 time then may be they have 100 (less or more ) ip addresses of tor servers . and after they will put this ip addresses in the system to check which clients are connecting or connected before to  this ip addresses , and after they will arrest the people very easy !!!!<br />
please if you really are thinking about internet freedom and wants to help people to access to free internet and safe internet just release the complete package of tor software (client and server ) for public use , after it will be 100000000 times safer specially for writers journalists and ..... because they will buy a vps (virtual private server ) or ded servers for themselves in the other countries and after they can connect to the spesific ip address that nobody knows any thing about that , and its not important to route over 10-20-or more servers .<br />
people in groups of 5- 10 can buy one vps for just 20 Us$ and pay 2-5 $ per month .</p>
<p>LOOK THIS PROGRAM WITH SPECIFIC SERVERS NEVER CAN BE SECURE BECAUSE ITS VERY EASY TO FIND WHO IS USING THIS PROGRAM .</p>
<p> i write this comment for the other projects before, and what i understand was this " people who was working on that projects just was thinking about money and they dont care about the people like every where people shows they are helping but in fact they are thinking about mony " i think   the only reason that stops releasing the server side program is money . i hope directors of this project release the server side program  for freedom<br />
if you dont like to release the current version of your server side you can release older version or one lite version ,<br />
the only thing we need is one program(server/client)  that can encrypt/decrypt the data with flexibility of using ports ,.   Not more</p>
<p>i hope that i see one answer</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-11000"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11000" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">August 28, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-10905" class="permalink" rel="bookmark">first excuse me because my</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-11000">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11000" class="permalink" rel="bookmark">I am not clear if you are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I am not clear if you are talking about Tor or not.  All of Tor's code is free, the exact same software that is a client can be configured to be a server in 2 clicks or a few lines in a configuration file. You may also notice the tor software is free, as in free of cost and free as in freedom. I'm confused as to why you think the server-side isn't the same as the client, and why you think we charge for any part of the software?</p>
<p>However, assuming you are talking about Tor.  Yes, if you do not use bridges, a sophisticated government can record every IP address connection between a user and the Internet. If they cross reference this list of IP addresses with Tor relays, they will see a user is possibly using Tor. However, this is one reason we developed bridges, <a href="https://torproject.org/docs/bridges.html.en" rel="nofollow">https://torproject.org/docs/bridges.html.en</a>. They are non-public tor relays that allow entry into the global Tor network.</p>
<p>Perhaps you should review how Tor works, <a href="https://torproject.org/about/overview.html.en#thesolution" rel="nofollow">https://torproject.org/about/overview.html.en#thesolution</a>.  You can already get tor, install it on a VPS, configure it as a non-published bridge, and give out that address to your friends. Lots of people do this already.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div></div></div><a id="comment-10558"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10558" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 20, 2011</p>
    </div>
    <a href="#comment-10558">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10558" class="permalink" rel="bookmark">I always wondered how the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I always wondered how the entry guard flag system works. Thanks for sharing.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-10559"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10559" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  rransom
  </article>
    <div class="comment-header">
      <p class="comment__submitted">rransom said:</p>
      <p class="date-time">August 20, 2011</p>
    </div>
    <a href="#comment-10559">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10559" class="permalink" rel="bookmark">That said, if choosing G=1</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><blockquote><p>That said, if choosing G=1 is a huge win for security, we should work on other ways to reduce variance.</p></blockquote>
<p>Every client needs to have at least two guard nodes, in two different 'families' and /16 netblocks.  If a client has only a single guard node, the set of exit nodes that it uses is likely to be distinguishable from that of other Tor clients.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-10565"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10565" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 21, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to rransom</p>
    <a href="#comment-10565">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10565" class="permalink" rel="bookmark">On the other side of this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>On the other side of this equation is the risk that somebody identifying your guards can use them as a fingerprint for you. There are an increasing set of papers that describe how to learn the relays in Alice's path (there's another one from Prateek Mittal coming up at CCS this year). If you can do the attack reliably over time, you can learn that whoever Alice is, she's picked these N relays as her guards. If G is small, this fingerprint is not very meaningful: plenty of other users have the same fingerprint. If G is large, I worry that some fingerprints will be rare.</p>
<p>As for G=1 vs G=2 and the risk that having only one guard could let a website fingerprint you as "that guy who never exits from 18.244.x.x", I think that's just another component of the tradeoff space that we need to evaluate.</p>
<p>I should note that if we use our current guard algorithm and G=1, we will automatically handle the case you note: if our exit node is in the same family/subnet as our first guard, we'll automatically move to the second guard in our list for that circuit. But rather than making you happy, that behavior should make you nervous about a new attack -- it means an observer watching Alice can discover when her chosen exit is in the same family as her primary guard. Isn't anonymity fun. :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-10564"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10564" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 21, 2011</p>
    </div>
    <a href="#comment-10564">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10564" class="permalink" rel="bookmark">Why trust an entry guard</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Why trust an entry guard more than any other relay node though?</p>
<p>If an entry guard is compromised, then how does "UseEntryGuards 1" not make matters even worse for a client than choosing an entry point at random?</p>
<p>It seems like common sense to me that if I wanted to identify clients by monitoring traffic flows from entrance to exit, that that would be best served by me running 'entry guards' and mandating all Tor users to utilize those entry points by default.  That's the prime position to be in, is it not?</p>
<p>I don't see how an entry point's uptime helps safeguard against this exploitation at all.  Doesn't it just mean they're that much more dedicated to achieving their aim, having put the time and resources in?</p>
<p>Isn't the argument that entry guards provide more stability at the expense of a greater chance of your security being compromised due to the concentration of clients towards these nodes?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-10568"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10568" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 22, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-10564" class="permalink" rel="bookmark">Why trust an entry guard</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-10568">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10568" class="permalink" rel="bookmark">Seems to me you&#039;re mixing</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Seems to me you're mixing two issues together.</p>
<p>The first issue is whether we should do the "guard" design for entry nodes at all. That is, whether Alice should pick a few relays and stick with them as the first hop for all her circuits. I think the answer there is a clear 'yes'. See the faq entry (and the papers it links to) for more details: <a href="https://www.torproject.org/docs/faq#EntryGuards" rel="nofollow">https://www.torproject.org/docs/faq#EntryGuards</a></p>
<p>The second issue is what subset of the relays should be acceptable choices as entry guards. I agree with what I think you're saying, in that I suspect the subset should be larger than it is now. But when you say "entry guards provide more stability at the expense of greater chance of compromise", you're missing the subtlety -- the stability actually improves anonymity, because less churn in your entry guards means fewer chances to pick a bad one. So the choice is not between stability and security; it's between defending against one attack vs defending against another attack. I think we are choosing a not-very-good point in this tradeoff currently; we should use historical Tor network data to help us recognize where the better points for the tradeoff are.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-10574"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10574" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 23, 2011</p>
    </div>
    <a href="#comment-10574">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10574" class="permalink" rel="bookmark">Doesn&#039;t the current concept</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Doesn't the current concept of sticking to a few entry guards for a long time sacrifies one for sure so the rest of the flock is save? The hunter is slowed down but Alice is a sitting duck when she gets in sight? </p>
<p>Instead of slowing down the hunter Tor could speed up the movement within the swarm. </p>
<p>I assume factors for correlation are connection time, transferred bytes and transfer time. There is little to be done about the connection time to the entry guard. Chance of correlation should increase with the number of bytes Alice receives over one circuit. </p>
<p>(Brainstorm mode on)</p>
<p>A: Changing first hops as soon a fixed number of bytes is transferred (which might be equal or smaller the size of an average web page). This number would be the same for all Tor users. </p>
<p>Or B: Individually, referring to the idea of virtual entry guards, by changing to the twin as soon as a random size of the current file is transferred (between 1/3 and 2/3).</p>
<p>Both A and B changes the number of bytes of a transferred file measured at adversary's entry guard. Transfer times for the file parts are also likely to differ by running over different first hops.</p>
<p>(Brainstorm mode off)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-10902"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10902" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 25, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-10574" class="permalink" rel="bookmark">Doesn&#039;t the current concept</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-10902">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10902" class="permalink" rel="bookmark">Conjecture #1: an attacker</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Conjecture #1: an attacker who sees traffic at both sides of the circuit can confirm pretty quickly that he's seeing two sides of the same circuit. There are a number of papers supporting this idea, e.g:<br />
<a href="http://freehaven.net/anonbib/#danezis:pet2004" rel="nofollow">http://freehaven.net/anonbib/#danezis:pet2004</a><br />
and basically none refuting it.</p>
<p>Conjecture #2: this attacker can confirm it in just a few packets or cells. This claim is shakier, in that there are fewer papers exploring it. But the rate of improvement for attack papers compared to defense papers makes me think it's unwise to doubt it. Or said another way, we're better off designing conservatively under the assumption it's true than designing a broken anonymity system on the hope that it's false.</p>
<p>For related discussion, see:<br />
<a href="http://freehaven.net/anonbib/#murdoch-pet2007" rel="nofollow">http://freehaven.net/anonbib/#murdoch-pet2007</a><br />
<a href="http://www.lightbluetouchpaper.org/2007/05/28/sampled-traffic-analysis-by-internet-exchange-level-adversaries/" rel="nofollow">http://www.lightbluetouchpaper.org/2007/05/28/sampled-traffic-analysis-…</a><br />
and<br />
<a href="http://freehaven.net/anonbib/#active-pet2010" rel="nofollow">http://freehaven.net/anonbib/#active-pet2010</a></p>
<p>Protecting against confirmation attacks is an open research topic that's gotten a *lot* of attention from researchers with basically no results. It's tough to produce a padding/morphing/etc design and also a proof that there are no practical attacks against it. Until somebody produces something more convincing than "surely this would work", I think it's best to assume we can't stop traffic confirmation attacks of any form.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-10575"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10575" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 23, 2011</p>
    </div>
    <a href="#comment-10575">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10575" class="permalink" rel="bookmark">Does the risk of correlation</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does the risk of correlation changes wether Alice is uploading or downloading? </p>
<p>If I understand the model right, choosing the best value for G depends on the ratio of how many nodes of the total are under control of an adversary. And this number is unknown and can only be estimated. </p>
<p>My guess is if less than half of the nodes are under control of an adversary then changing between more entry guards is better.<br />
Maybe a mathematician can chime in and calculate the probabilities for different percentages of entry nodes under adversary control. </p>
<p>Another guess, if Alice uses only a part of her time on the Tor network for critical work she is better of with a higher number of entry guards.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-10849"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-10849" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 24, 2011</p>
    </div>
    <a href="#comment-10849">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-10849" class="permalink" rel="bookmark">because don&#039;t implemented</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>because don't implemented tor a function, a menu that allows you to download videos such as those of youtube, not through a computer tor, but dividing it on multiple computers, and then reassembled on the destination computer. For example you can use a program that can download the movie, only in a certain period of time, once reassembled at the destination, you can play through vlc. In this way the connection<br />
is ripard also gives access to movies, which generally is slow and penalizing,</p>
<p>Why not do the same thing with FTP files,  allowing the user decide whether to enable the tor to the function?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11270"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11270" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 29, 2011</p>
    </div>
    <a href="#comment-11270">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11270" class="permalink" rel="bookmark">Should there be some</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Should there be some advanced weighting mechanism for users<br />
to specify their adversary? Yet not allow obvious foot shooting?<br />
Such as country, AS, provider, etc. I know this has come up<br />
before, just not sure where. And it would certainly depend on<br />
some new metadata or lookup method too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-11276"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11276" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">August 30, 2011</p>
    </div>
    <a href="#comment-11276">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11276" class="permalink" rel="bookmark">Do those entry guards</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do those entry guards persist as one moves from home to office, to airport, to internet cafe?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-11284"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-11284" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">August 30, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-11276" class="permalink" rel="bookmark">Do those entry guards</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-11284">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-11284" class="permalink" rel="bookmark">Yes. It would be nice to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes. It would be nice to build "profiles" or something where Tor recognizes that now you're the office you and it switches to those guards. But how do we do that without keeping a nice set of profiles on disk, which is bad for other reasons? We don't want to just throw out the guards when you change location, since that would substantially defeat the purpose of them in the first place.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
