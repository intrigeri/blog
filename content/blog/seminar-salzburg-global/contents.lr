title: A seminar at Salzburg Global
---
pub_date: 2009-10-08
---
author: phobos
---
tags:

censorship circumvention
salzburg global
strengthening independent media
new media
old media
digital natives
analog elders
---
categories: circumvention
---
_html_body:

<p>I was invited to attend a seminar on "Seeding Tomorrow's Media:  Strategies for More Effective Engagement and Investment".  This invite came with an all expense paid trip to the <a href="http://sim.salzburgglobal.org/" rel="nofollow">Salzburg Global Trust</a> in Salzburg, Austria.  The group was an interesting mix of "new media" bloggers and activists and "old media" foundations and journalists.  We decided "new media vs old media" is a false dichotomy; perhaps community vs. institutionalized media is a more apt description.  </p>

<p>We discussed a number of topics, however the most common was the current funding expectations from US AID, UK DFID, and a number of Foundations versus what people in each locale need and are actually looking to do.  The concerns of censorship and state control of media as it moves online were discussed.  Most of this group had heard of Tor, but not fully understood our role in providing the needed anonymity and privacy for journalists around the world.  Entirely too many citizen and professional journalists run afoul of the apparatus of state media control too easily.  These people then find themselves being censored, blocked, or worse, due to their articles appearing online; whether in blogs, newspapers, microblogging, etc.</p>

<p>Overall, it was a great trip.  Salzburg is beautiful. The people and content were great.  This isn't the typical conference Tor attends, however any conference with intelligent conversation is a fine place to be.</p>

<p>Some examples of the discussion over the past few days are:</p>

<ul>
<li><a href="http://sim.salzburgglobal.org/blog/2009/10/02/first-do-no-harm-0" rel="nofollow">First Do No Harm</a></li>
<li><a href="http://sim.salzburgglobal.org/blog/2009/10/07/new-era-media-development-part-i" rel="nofollow">New Era Media Development</a></li>
<li><a href="http://sim.salzburgglobal.org/blog/2009/10/07/quality-and-critical-mass" rel="nofollow">Quality and Cricital Mass</a></li>
<li><a href="http://sim.salzburgglobal.org/blog/2009/10/07/sim-city" rel="nofollow">SIM City</a></li>
</ul>

