title: October 2008 Progress Report
---
pub_date: 2008-12-02
---
author: phobos
---
tags:

vidalia
crashes
torbutton
china
translations
censorship circumvention
farsi
auto-updater
---
categories:

applications
circumvention
global south
localization
---
_html_body:

<p><strong>Design</strong><br />
We continued enhancements to the Chinese and Russian Tor website translations. We also have a second Chinese translator for the website now, so hopefully we will get more prompt translations there. Our Farsi translation from this summer is slowly becoming obsolete; we should solve that at some point.</p>

<p>We added a new "30 second summary" web page for Tor:<br />
<a href="https://www.torproject.org/30seconds" rel="nofollow">https://www.torproject.org/30seconds</a><br />
and a new "easy download" page since the original is so complex:<br />
<a href="https://www.torproject.org/easy-download" rel="nofollow">https://www.torproject.org/easy-download</a></p>

<p>In the upcoming Vidalia 0.2.0 development release:<br />
  - Support changing UI languages without having to restart Vidalia.<br />
  - Updated Czech, Polish, Romanian and Turkish translations.</p>

<p>In the upcoming Vidalia 0.1.10 stable release:<br />
  - Add a prettier dialog for prompting people for their control port password that also includes a checkbox for whether the user wants Vidalia to remember the entered password, a Help button, and a Reset button (Windows only).<br />
  - Fix a crash bug that occurred when the user clicks 'Clear' in the message log toolbar followed by 'Save All'.<br />
  - Uncheck the Torbutton options by default in the Windows bundle installer if Firefox is not installed.<br />
  - Add an Windows bundle installer page that warns the user that they should install Firefox, if it looks like they haven't already done so.</p>

<p>It looks like Australia is soon to be joining the ranks of countries with a nationwide filtering regime:<br />
<a href="http://arstechnica.com/news.ars/post/20081016-net-filters-required-for-all-australians-no-opt-out.html" rel="nofollow">http://arstechnica.com/news.ars/post/20081016-net-filters-required-for-…</a></p>

<p><strong>Proposals</strong><br />
We finished the first iteration of our auto-updater spec:<br />
<a href="https://svn.torproject.org/svn/updater/trunk/specs/thandy-spec.txt" rel="nofollow">https://svn.torproject.org/svn/updater/trunk/specs/thandy-spec.txt</a><br />
We detail our current auto-updater progress below.</p>

<p>Proposal 156 (Tracking blocked ports on the client side) moves us closer to having clients be able to automatically detect which ports are blocked by their local firewall, so they can bootstrap faster and avoid picking entry guards that aren't reachable for them. The the next steps here are to a) decide if this overall approach is the right approach, and b) revise the patch to be more memory-friendly.<br />
<a href="https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/156-tracking-blocked-ports.txt" rel="nofollow">https://svn.torproject.org/svn/tor/trunk/doc/spec/proposals/156-trackin…</a></p>

<p><strong>Advocacy</strong><br />
Roger started a "Brainstorming about Tor, Germany, and data retention" thread on or-dev:<br />
<a href="http://archives.seul.org/or/dev/Oct-2008/msg00001.html" rel="nofollow">http://archives.seul.org/or/dev/Oct-2008/msg00001.html</a><br />
which eventually turned into a blog post:<br />
<a href="https://blog.torproject.org/blog/tor%2C-germany%2C-and-data-retention" rel="nofollow">https://blog.torproject.org/blog/tor%2C-germany%2C-and-data-retention</a><br />
as well as a (rejected) 25C3 submission. While I had originally been thinking of the issue in terms of what the ISP of a Tor relay might do, the discussion also came up about what responsibilities a Tor relay operator has with respect to the vague new data retention laws:<br />
<a href="http://archives.seul.org/or/talk/Oct-2008/threads.html#00126" rel="nofollow">http://archives.seul.org/or/talk/Oct-2008/threads.html#00126</a><br />
The ultimate result was a clarified perspective on logging inside Tor:<br />
<a href="http://archives.seul.org/or/talk/Oct-2008/msg00274.html" rel="nofollow">http://archives.seul.org/or/talk/Oct-2008/msg00274.html</a></p>

<p>We finally tracked down and solved the mysterious DoS attacks on some of the Tor directory authorities:<br />
<a href="http://archives.seul.org/or/talk/Oct-2008/msg00056.html" rel="nofollow">http://archives.seul.org/or/talk/Oct-2008/msg00056.html</a></p>

<p>We started chatting with Aaron about his "tor2web" proxy idea for letting non-Tor users access hidden service content:<br />
<a href="http://tor.theinfo.org/" rel="nofollow">http://tor.theinfo.org/</a><br />
Somebody should follow up on that more to encourage him to keep at it.</p>

<p>Announced Joel Reardon's thesis on or-talk, and followed up with him to point him to some pieces of anonbib he needs to read more, to tell him about 25C3, and to remind him to publish his new measurement tools lest they become lost to time. </p>

<p>Roger and Karsten got the patches from proposal 155 into svn, and ultimately into the upcoming 0.2.1.7-alpha release. These were the bulk of the October progress for that NLnet project:<br />
<a href="https://www.torproject.org/projects/hidserv.html.en#Oct08" rel="nofollow">https://www.torproject.org/projects/hidserv.html.en#Oct08</a></p>

<p>Mike deleted the router-stability file for his directory authority (ides), which should provide temporary relief from bug 696 (which was causing most of the Stable flags to be assigned wrong, and in turn was causing instant messaging and related connections over Tor to be way more flaky than they should be):<br />
<a href="https://bugs.torproject.org/flyspray/index.php?do=details&amp;id=696" rel="nofollow">https://bugs.torproject.org/flyspray/index.php?do=details&amp;id=696</a><br />
If his router-stability file gets corrupted again, we will have learned something.</p>

<p>Roger, Jacob, and Mike went to the Google Summer of Code Mentor Summit on Oct 24-26 in Mountain View, where we met with a few hundred other GSoC mentors and generally shared information about Tor and how to make good use of summer students working on free software tools.</p>

<p>We also went to dinner with Niels Provos while we were there, to talk about options for the "Google gives you a captcha if you're using Tor" problem. It looks like the right answer there will be for Torbutton to automate some workaround.</p>

<p>Andrew started working with Jillian York, so she can start blogging about the great uses of Tor. More news in November, e.g.<br />
<a href="https://blog.torproject.org/blog/knight-pulse%2C-jillian%2C-and-tor" rel="nofollow">https://blog.torproject.org/blog/knight-pulse%2C-jillian%2C-and-tor</a></p>

<p>Matt Edman printed Vidalia T-shirts, and sent them out to the folks who have helped work on Vidalia lately. He is also working with a volunteer to clean up the Vidalia website, make new logos, clean up the installer graphics, etc.</p>

<p>Andrew wrote a blog post about anonymity in South Korea:<br />
<a href="https://blog.torproject.org/blog/online-anonymity-debate-south-korea" rel="nofollow">https://blog.torproject.org/blog/online-anonymity-debate-south-korea</a></p>

<p><strong>Distribution</strong><br />
Work on the Tor VM project continues. We have a working prototype available now with a walk-through and screenshots:<br />
<a href="http://peertech.org/files/demo/testinfo.html" rel="nofollow">http://peertech.org/files/demo/testinfo.html</a></p>

<p>We plan to release a more public alpha installer in November.</p>

<p><strong>Scalability</strong><br />
From the Tor 0.2.1.7-alpha ChangeLog:<br />
"The "ClientDNSRejectInternalAddresses" config option wasn't being consistently obeyed: if an exit relay refuses a stream because its exit policy doesn't allow it, we would remember what IP address the relay said the destination address resolves to, even if it's an internal IP address. Bugfix on 0.2.0.7-alpha; patch by rovv."</p>

<p><strong>Packaging</strong><br />
We changed our auto update design from code-name Glider to code-name Thandy, since there's a World of Warcraft cheat program named Glider and it might be a problem for WoW players that try to use Tor.</p>

<p>We've got the PKI and server-side for the auto updater in place. We wrote up a howto walking through how to set up the server-side for the updater, including how to assign roles and generate keys:<br />
<a href="https://svn.torproject.org/svn/updater/trunk/doc/HOWTO" rel="nofollow">https://svn.torproject.org/svn/updater/trunk/doc/HOWTO</a></p>

<p>We've also decided that Python should work fine for the client-side too. Mike found some techniques to include only exactly the python libs we need, rather than the whole mess of python libs:<br />
<a href="http://www.py2exe.org/index.cgi/BetterCompression" rel="nofollow">http://www.py2exe.org/index.cgi/BetterCompression</a><br />
and Martin has been messing with saving some additional space by sharing the openssl lib between Tor and Thandy.</p>

<p>The next steps for November are:<br />
 - Roger is going to figure out what PKI we want for the first round of testing (what roles, which keys, how many, who, etc), and deploy a Thandy server so we can put some basic packages on it for testing.<br />
 - Nick is going to finish the client-side of Thandy, in terms of teaching it how to decide which packages and bundles are out of date, and teaching it to download new files and check all the right signatures.<br />
 - Martin is going to package Thandy plus all the right python libs in an easy Windows exe that hopefully isn't too big.<br />
 - Matt Edman is going to add a simple interface to Vidalia for client-side Thandy configuration: stuff like a GUI for telling the user that new updates have appeared and letting the user click "yes, please update me now", etc.<br />
 - Nick and Matt are going to brainstorm more about the interface between Vidalia and Thandy. For example, which program should keep state about the versions of each package that are installed, which program should be responsible for noticing if an install or upgrade attempt fails, etc.</p>

<p>All the steps but the last I think are going to be pretty straightforward. This last step has the most potential pitfalls in it, since we're trying to keep Thandy general and platform-independent yet *something* (either Thandy or Vidalia, or something in between) has to tackle all the crazy Windows-specific pieces.</p>

<p>It also looks like we should move the Tor packages and bundles from NSIS (Nullsoft installer) to MSI installer, as MSI can handle versioning and automatic installs (and uninstalls!) more gracefully. It's not yet clear yet if we're going to try to squeeze that installer shift into the November development timeframe.</p>

<p><strong>Tor Browser Bundle</strong><br />
We've started to think about moving the Tor Browser Bundle from Firefox 2 to Firefox 3. This will mean we should measure new traces. We'll do it once Torbutton is known to be more stable on Firefox 3, which should happen in early 2009.</p>

---
_comments:

<a id="comment-431"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-431" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2008</p>
    </div>
    <a href="#comment-431">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-431" class="permalink" rel="bookmark">I don&#039;t know why you&#039;re</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I don't know why you're bothering to update Torbutton at all.  It completely disables Firefox, and is therefore completely worthless.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-433"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-433" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 14, 2008</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-431" class="permalink" rel="bookmark">I don&#039;t know why you&#039;re</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-433">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-433" class="permalink" rel="bookmark">it disables anonymity</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>it disables anonymity attacks.  if you're life and job depend on being anonymous, torbutton is fantastic.  if you just want to browse privately, use relakks or anonymizer or some other thing to hide traffic.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-613"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-613" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>reddog (not verified)</span> said:</p>
      <p class="date-time">February 11, 2009</p>
    </div>
    <a href="#comment-613">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-613" class="permalink" rel="bookmark">password</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>downloaded the new version of tor 2.0.34. but it insists on asking for a password that i don't have. What do I do?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-668"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-668" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">February 15, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-613" class="permalink" rel="bookmark">password</a> by <span>reddog (not verified)</span></p>
    <a href="#comment-668">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-668" class="permalink" rel="bookmark">FAQ Answer</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><a href="https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#TorPasswordPrompt" rel="nofollow">https://wiki.torproject.org/noreply/TheOnionRouter/TorFAQ#TorPasswordPr…</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-832"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-832" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 18, 2009</p>
    </div>
    <a href="#comment-832">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-832" class="permalink" rel="bookmark">Sync TorButton and JonDoFox headers?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Is there any chance of syncing TorButton and JonDoFox headers (user agent, etc.)?</p>
<p>Possible benefits:<br />
* Tor users could choose whether they prefer to use TorButton or JonDoFox, but still get the same headers either way. JonDoFox already includes a Tor setting in its proxy switcher.<br />
* JonDoFox is fully Firefox 3 compatible.<br />
* The JonDos team does not change their settings very often, which makes it easier for a user of an alternate browser, such as Lynx or Konqueror, to attempt to duplicate the headers without constant maintenance.<br />
* Collaboration on deciding what the optimum settings are.<br />
* Anyone who uses both Tor and JonDo would be able to securely use the same settings for both - aside from the proxy setting, of course.</p>
</div>
  </div>
</article>
<!-- Comment END -->
