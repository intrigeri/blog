title: New Release: Tor Browser 11.5.7 (Windows, macOS, Linux)
---
pub_date: 2022-11-03
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 11.5.7 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 11.5.7 is now available from the [Tor Browser download page](https://www.torproject.org/download/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/11.5.7/).

This is a minor release for desktop platforms intended to facilitate the single-locale to multi-locale bundle upgrade coming with the 12.0 series later this month.

The full changelog since [Tor Browser 11.5.6](https://gitweb.torproject.org/builders/tor-browser-build.git/plain/projects/tor-browser/Bundle-Data/Docs/ChangeLog.txt?h=maint-11.5) is:

- Windows + macOS + Linux
  - [Bug tor-browser#41413](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41413): Backup intl.locale.requested in 11.5.x
