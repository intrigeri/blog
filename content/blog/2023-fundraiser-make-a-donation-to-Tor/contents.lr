title: If you value Tor, please make a donation
---
author: alsmith
---
pub_date: 2023-10-16
---
categories:
fundraising
---
summary: 
The Tor Project runs on your contributions. Every year, we hold a fundraiser where we ask for your support. Please consider making a donation to support the valuable work we are doing.
---
body:

Right now, the Tor network, Tor Browser, onion services, Snowflake, and the ecosystem of tools and services built and maintained by the Tor Project are protecting the privacy of millions of people. Because the Tor Project is a nonprofit, this work is powered by donations from our community – by you.

In that vein, today we're launching our annual fundraising campaign. If you've been a Tor supporter for a while, you probably know that this is the moment we add a new message to about:tor, a banner to the torproject.org sites, and use our social channels to highlight how your support empowers people all over the world to exercise their right to privacy. 

This year we're keeping our message simple: **if you value the privacy that Tor provides to yourself or to other people, [please make a donation](https://torproject.org/donate/donate-bp1-2023).** Your support ensures that the Tor Project remains strong on an organizational level, and that the ecosystem of Tor services and tools continue to reach the people who need privacy online the most.

![From Tor Browser to Onion Services to bridges, user and community support to dedicated anti-censorship teams, we are set up to quickly spring into action when the need arises. Help us keep it that way!](yec-ecosystem-blog.png)

## The Tor Project faces some unprecedented challenges this year. 

Last year, [charitable giving from individuals from the U.S. decreased by 10.5%](https://philanthropy.iupui.edu/news-events/news-item/giving-usa:-total-u.s.-charitable-giving-declined-in-2022-to-$499.33-billion-following-two-years-of-record-generosity.html?id=422) compared to the year prior---this has only happened four times in U.S. history since 1956. We can confirm that this trend is real, and we've seen it continue throughout 2023 and across our network of global supporters. Widespread [tech-sector layoffs](https://techcrunch.com/2023/09/19/tech-industry-layoffs-2023/) also mean that hundreds of thousands of people have unexpectedly lost their jobs---and this has disproportionately impacted our community. We know now is a difficult time for many people who use and love Tor.

Even in difficult economic conditions, **Tor is and will always be free**. Unrestricted access to the technology we create is part of our mission. But the challenges of 2023 and beyond mean that if you are in the position to donate this year, your support is more vital than ever. 

If you value the privacy that Tor offers yourself and others, [please make a donation today](https://torproject.org/donate/donate-bp1-2023). Your contribution will help ensure Tor continues to provide online privacy to everyone who needs it, and help us reach our ambitious goals during a difficult economic time.

![Tor is free and always will be](yec-free-blog.png)

## Ways to contribute

* **Can you make a recurring donation?** [Set up a monthly donation](https://torproject.org/donate/donate-bp1-2023). This is the best, most impactful way to help the Tor Project budget during uncertain times.

* **Can you make a one-time gift?** Make a [donation through our website by credit card or PayPal](https://torproject.org/donate/donate-bp1-2023); through the mail by cash, check, or money order; through a stock donation or bank transfer; and [many more options listed in our FAQ](https://donate.torproject.org/donor-faq/).

* **Do you have a company or organization that uses Tor or believes in Tor?** [Become a member](https://torproject.org/about/membership).

* **Does your company match donations made by employees?** Make a donation and file the paperwork with your organization so they match your gift. Let us know if you need help!

* **Do you have cryptocurrency to give?** [We can accept donations in ten different coins](https://donate.torproject.org/cryptocurrency).

![User quote: "I don't want to live in a world where everything I say, everything I do, everyone I talk to, every expression of creativity or love or friendship is recorded.
Thank you, Tor Project."](yec-quote-blog.png)
