title: Stem Release 1.4
---
pub_date: 2015-05-13
---
author: atagar
---
tags: stem
---
_html_body:

<p>Greetings wonderful carbon-based residents of the Internet. I'm pleased to announce the 1.4.0 release of Stem!</p>

<p>What is Stem, you ask? For those who aren't familiar with it Stem is a Python library for interacting with Tor. With it you can script against your relay, descriptor data, or even write applications similar to Nyx and Vidalia.</p>

<p><b><a href="https://stem.torproject.org/" rel="nofollow">https://stem.torproject.org/</a></b></p>

<p>So what's new in this release?</p>

<h2>Ephemeral Hidden Services and Descriptors</h2>

<p>Tor's 0.2.7.1 release is bringing with it new hidden service capabilities, most notably ADD_ONION and HSFETCH. Ephemeral hidden services let you easily <a href="https://stem.torproject.org/tutorials/over_the_river.html#ephemeral-hidden-services" rel="nofollow">operate a hidden service that never touches disk</a>.</p>

<p>This latest Tor release also brought with it the ability to retrieve a hidden service's descriptor information. Stem knows how to <a href="https://stem.torproject.org/tutorials/over_the_river.html#hidden-service-descriptors" rel="nofollow">parse, validate, and decrypt these documents</a>.</p>

<h2>Faster Descriptor Parsing</h2>

<p>When reading descriptors without validation (which is the new default), documents are now lazily parsed. This provides a very substantial speedup depending on the document's type...</p>

<ul>
<li>Server descriptors: 27% faster</li>
<li>Extrainfo descriptors: 71% faster</li>
<li>Microdescriptors: 43% faster</li>
<li>Consensus: 37% faster</li>
</ul>

<p>Prefer to keep validation? No problem! Just <a href="https://stem.torproject.org/tutorials/mirror_mirror_on_the_wall.html#validating-the-descriptors-content" rel="nofollow">include '<b>validate = True</b>'</a> and<br />
you'll be good to go.</p>

<p>As always this is just the tip of the iceberg. For a full rundown on the myriad of improvements and fixes in this release see...</p>

<p><b><a href="https://stem.torproject.org/change_log.html#version-1-4" rel="nofollow">https://stem.torproject.org/change_log.html#version-1-4</a></b></p>

---
_comments:

<a id="comment-93367"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93367" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 13, 2015</p>
    </div>
    <a href="#comment-93367">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93367" class="permalink" rel="bookmark">Tor is doing great work</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Tor is doing great work</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93395"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93395" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 13, 2015</p>
    </div>
    <a href="#comment-93395">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93395" class="permalink" rel="bookmark">Does arm also need an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Does arm also need an update?  (Maybe to replace Vidalia in a future Tails?)</p>
<p>If so a bit more documentation of arm would be appreciated.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-93437"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93437" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">May 13, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-93395" class="permalink" rel="bookmark">Does arm also need an</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-93437">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93437" class="permalink" rel="bookmark">It does indeed need an</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It does indeed need an update. I think Damian is working on stem first, and arm next.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-93758"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93758" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  atagar
  </article>
    <div class="comment-header">
      <p class="comment__submitted">atagar said:</p>
      <p class="date-time">May 17, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-93395" class="permalink" rel="bookmark">Does arm also need an</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-93758">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93758" class="permalink" rel="bookmark">Yup! Arm rewrite is in the</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yup! Arm rewrite is in the works and has been for quite some time. It's a big project but it's pretty far along and I hope to have a shiny, new release ready later this year.</p>
<p>Each month I give an update on its progress. You can follow along at...</p>
<p><a href="http://blog.atagar.com/" rel="nofollow">http://blog.atagar.com/</a></p>
<p>(btw, arm was renamed recently to nyx - sorry about any confusion that causes!)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-93504"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93504" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 14, 2015</p>
    </div>
    <a href="#comment-93504">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93504" class="permalink" rel="bookmark">That is good news!  Thank</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>That is good news!  Thank you both.</p>
<p>The current arm documention is IMHO insufficient to help most people use arm effectively.  Most of us are primarily interested in a replacement for Vidalia.  Text based is fine by me.</p>
<p>While using Tails 4.3 (which still has Vidalia which is showing its age, is apparently not maintained upstream, and has apparently not scaled well), I think I see separate Tor circuits doing OCSP lookups and connecting to the website itself.   If so, does that address the longstanding concern that NSA might be tracking Tor users by tracking OCSP lookups when they navigate to an https site?</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-93759"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93759" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  atagar
  </article>
    <div class="comment-header">
      <p class="comment__submitted">atagar said:</p>
      <p class="date-time">May 17, 2015</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-93504" class="permalink" rel="bookmark">That is good news!  Thank</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-93759">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93759" class="permalink" rel="bookmark">Yup! Agreed, we need a new</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yup! Agreed, we need a new graphical controller.</p>
<p>My plan around arm (aka nyx) was to make modern controller library (Stem), then write a new version of arm that uses it to ensure it's up to snuff for a new graphical controller.</p>
<p>I'm pleased to say this year we *are* taking on that project. Not a GUI like Vidalia, but rather a web dashboard. This winter I'm mentoring Cristobal who's writing a web dashboard for relay operators. Think arm, but ajax when you go to localhost...</p>
<p><a href="https://leivaburto.github.io/sop-proposal/" rel="nofollow">https://leivaburto.github.io/sop-proposal/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-93733"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-93733" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">May 17, 2015</p>
    </div>
    <a href="#comment-93733">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-93733" class="permalink" rel="bookmark">Re updating arm controller</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Re updating arm controller (as a replacement for Vidalia in Tails): </p>
<p>It is useful for some Tor users to run a utility with functionality similar to Vidalia, although a text based one like arm would be fine for me.  Tails still uses Vidalia which I understand has not been maintained.  A longstanding issue is that when Vidalia starts the CPU works very hard (100% of capacity).  If Vidalia is somehow maliciously caused to restart frequently this could become a kind of DOS.  Indeed, I have been experiencing great difficulty in using Tails 1.4 booted from a DVD burned from a verified iso image.   Boots normally and using netstat I can see initial connections to DirAuths, but after a few minutes:</p>
<p>1. gnome connection manager applet pops up "disconnected"</p>
<p>2. (seconds later) "connected"</p>
<p>3. (ten seconds later) "Tor is ready"</p>
<p>This happens repeatedly, even if not doing anything, but every few minutes if using Tor Browser or OFTC chat.</p>
<p>Using bridges seems to help, but ISP claims any problems must be upstream from their network.  It is possible the problem is somehow connected to my hardware (computer, router) but I can find no real evidence of that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
