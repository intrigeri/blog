title: Tor Weekly News — July, 24th 2013
---
pub_date: 2013-07-24
---
author: lunar
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the 4th issue of Tor Weekly News, the weekly newsletter that covers what is happening in the great Tor community.</p>

<p>The next newsletter is going to be posted to the resurrected <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">tor-news mailing-list</a>. Just subscribe!</p>

<h1>Summer Development Meeting 2013</h1>

<p>About 40 core Tor contributors gathered over July 22-23 at the <a href="http://www.tum.de/" rel="nofollow">Technische Universität München</a> in Germany for <a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2013SummerDevMeeting" rel="nofollow">this year's Summer Development Meeting</a>. That's quite a number, and as could be expected, there are a huge number of topics up for discussion — the brainstorming session generated around 150. Gunner from <a href="http://aspirationtech.org/" rel="nofollow">Aspiration Tech</a> is once again facilitating the meeting, helping everyone focus on the most pressing issues.</p>

<p>Rough notes from some of the break-out sessions are now up on the wiki. A list of the topics and a brief summary of the discussions are given below:</p>

<ul>
<li><a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2013SummerDevMeeting/LoveForVolunteers" rel="nofollow">Love for volunteers</a>: strategies for outreach and long-term support; identifying where they're most needed</li>
<li><a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2013SummerDevMeeting/PluggableTransports0" rel="nofollow">Censorship and pluggable transports</a>: documenting the now-large number of pluggable transports; discussing countries' differing censorship strategies</li>
<li><a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2013SummerDevMeeting/Fundraising" rel="nofollow">Fundraising</a>: funding outlook seems to be positive, although diversity in funding sources is needed to reduce dependency on single institutions like the US Government</li>
<li><a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2013SummerDevMeeting/WebsiteTranslation" rel="nofollow">Website Translation</a>: notes from a discussion which led to the definition of a new translation review process and a list of actionables</li>
<li><a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2013SummerDevMeeting/ServiceInfrastructure" rel="nofollow">Service infrastructure</a>: multiple fundamental services could use some more love: GetTor (TBB by email), website, Trac, check.tp.org, BridgeDB; shutting down unmaintained services</li>
<li><a href="https://trac.torproject.org/projects/tor/wiki/org/meetings/2013SummerDevMeeting/OpSec" rel="nofollow">Operational security</a>: random notes on OpSec for Tor people</li>
</ul>

<p>On the sidelines of the dev meeting, Roger and Jake are doing a public talk on the topic of <a href="https://gnunet.org/tor2013tum" rel="nofollow">Tor and the Censorship Arms Race</a>.</p>

<p>If you want to meet people who spend their day making Tor a reality, you can <a href="https://blog.torproject.org/blog/join-us-tor-hack-day-munich-germany" rel="nofollow">join them for a public hack day</a> on Friday, July 26, 2013. Bring your ideas, questions, projects, and technical expertise with you!</p>

<h1>Remote descriptor fetching in Stem</h1>

<p><a href="https://lists.torproject.org/pipermail/tor-dev/2013-July/005156.html" rel="nofollow">Damian Johnson announced</a> having implemented remote descriptor fetching in <a href="https://stem.torproject.org/" rel="nofollow">Stem</a>, a feature to <a href="https://lists.torproject.org/pipermail/tor-dev/2013-May/004924.html" rel="nofollow">migrate more metrics measurement tools to Python</a>. “Its usage is pleasantly simple” wrote Damian. See for yourself in the <a href="https://stem.torproject.org/api/descriptor/remote.html" rel="nofollow">excellent documentation</a>!</p>

<h1>Orbot 12.0.1 call for beta testing</h1>

<p>After a long interval, Nathan Freitas <a href="https://lists.torproject.org/pipermail/tor-talk/2013-July/029063.html" rel="nofollow">announced the release of a new version of Orbot</a> — a client for the Tor network on Android mobile devices. For Orbot 12.0.1, the developers have “switched versioning styles to a simpler major.minor.bugfix model”, wrote Nathan.</p>

<p>He continues with a call for testing: “Since we haven't done a release in a while, and we have some new build tools, I mostly want to make sure I have not done something terribly wrong in the build process. Please confirm back if you are able to successfully use this release.”</p>

<p>Updates in 12.0.1:</p>

<ul>
<li>Updated to Tor 0.2.4.15-RC</li>
<li>flashy screen bug fixed now shows traffic</li>
<li>Stats in notification area</li>
<li>better handling of preference settings</li>
<li>Changes added superuser permission for Cyanogen</li>
</ul>

<p>You can <a href="https://guardianproject.info/releases/Orbot-release-12.0.1-beta-1.apk" rel="nofollow">download and start testing this release</a>.</p>

<h1>Miscellaneous development news</h1>

<p>Lunar reported on the <a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000292.html" rel="nofollow">trip to Brussels for LSM 2013</a>. To sum it up, “people are really supportive of what we do these days”.</p>

<p>anonym outlined the <a href="https://mailman.boum.org/pipermail/tails-dev/2013-July/003292.html" rel="nofollow">release schedule for Tails 0.20</a>.</p>

<p>intrigeri announced that <a href="https://mailman.boum.org/pipermail/tails-dev/2013-July/003297.html" rel="nofollow">Tails has switched to a more conventional task manager</a>. It might now be easier for you to see what needs to be done. Have a look at the current list of 496 <a href="https://labs.riseup.net/code/projects/tails/issues" rel="nofollow">open issues</a>, there might be something waiting just for you.</p>

<p>Tor developers would love to find an easier way to debug tor when the daemon crashes. Nick Mathewson came up with an initial piece of code that could (when complete) <a href="https://bugs.torproject.org/9299" rel="nofollow">dump stack traces</a> on assertion, crash, or general trouble to the logs. There's more work to be done before it can be merged in Tor. Feel free to help — especially if you know how to do this on Windows or BSD.</p>

<p>Nick Mathewson along with Andrea created a wiki page for <a href="https://trac.torproject.org/projects/tor/wiki/org/roadmaps/Tor/025/TicketTriage025" rel="nofollow">initial 0.2.5 series ticket triage</a>. If you have your own agenda for 0.2.5, you should be talking to them now!</p>

<p><a href="https://lists.torproject.org/pipermail/tor-dev/2013-July/005158.html" rel="nofollow">Kostas Jakeliunas reported on their GSoC project</a> about producing a searchable metrics archive.</p>

<p><a href="https://lists.torproject.org/pipermail/tor-reports/2013-July/000291.html" rel="nofollow">Arturo reported</a> on his activities on Ooni probe in June.</p>

<p><a href="https://lists.torproject.org/pipermail/tor-dev/2013-July/005161.html" rel="nofollow">Pierre Lalet announced</a> that he started working on an implementation of the Tor protocol in Python. The intent is to have an independent implementation to test Tor. As the author puts it, "the purpose is NOT to implement a secure or robust implementation that could be an alternative to Tor." Have a look at <a href="https://github.com/cea-sec/TorPylle" rel="nofollow">the code</a>: "Comments, fixes and questions welcome!"</p>

<p>This issue of Tor Weekly News has been assembled by Lunar, dope457, harmony, moskvax, malaparte, whabib, and David Fifield.</p>

<p>Want to continue reading TWN? Please help us create this newsletter.  We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing-list</a> if you want to get involved!</p>

