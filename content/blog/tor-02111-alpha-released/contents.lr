title: Tor 0.2.1.11-alpha released
---
pub_date: 2009-01-22
---
author: phobos
---
tags:

alpha release
security fixes
bug fixes
---
categories: releases
---
_html_body:

<p>Tor 0.2.1.11-alpha finishes fixing the "if your Tor is off for a week it<br />
will take a long time to bootstrap again" bug. It also fixes an important<br />
security-related bug reported by Ilja van Sprundel. You should upgrade.<br />
(We'll send out more details about the bug once people have had some<br />
time to upgrade.)</p>

<p><a href="https://www.torproject.org/download.html.en" rel="nofollow">https://www.torproject.org/download.html.en</a></p>

<p>Changes in version 0.2.1.11-alpha - 2009-01-20<br />
<strong>Security fixes:</strong></p>

<ul>
<li>Fix a heap-corruption bug that may be remotely triggerable on<br />
      some platforms. Reported by Ilja van Sprundel.</li>
</ul>

<p><strong>Major bugfixes:</strong></p>

<ul>
<li>Discard router descriptors as we load them if they are more than<br />
      five days old. Otherwise if Tor is off for a long time and then<br />
      starts with cached descriptors, it will try to use the onion<br />
      keys in those obsolete descriptors when building circuits. Bugfix<br />
      on 0.2.0.x. Fixes bug 887.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>Try to make sure that the version of Libevent we're running with<br />
      is binary-compatible with the one we built with. May address bug<br />
      897 and others.</li>
<li>Make setting ServerDNSRandomizeCase to 0 actually work. Bugfix<br />
      for bug 905. Bugfix on 0.2.1.7-alpha.</li>
<li>Add a new --enable-local-appdata configuration switch to change<br />
      the default location of the datadir on win32 from APPDATA to<br />
      LOCAL_APPDATA. In the future, we should migrate to LOCAL_APPDATA<br />
      entirely. Patch from coderman.</li>
</ul>

<p><strong>Minor bugfixes:</strong></p>

<ul>
<li>Make outbound DNS packets respect the OutboundBindAddress setting.<br />
      Fixes the bug part of bug 798. Bugfix on 0.1.2.2-alpha.</li>
<li>When our circuit fails at the first hop (e.g. we get a destroy<br />
      cell back), avoid using that OR connection anymore, and also<br />
      tell all the one-hop directory requests waiting for it that they<br />
      should fail. Bugfix on 0.2.1.3-alpha.</li>
<li>In the torify(1) manpage, mention that tsocks will leak your<br />
      DNS requests.</li>
</ul>

<p>Original announcement can be found at <a href="http://archives.seul.org/or/talk/Jan-2009/msg00171.html" rel="nofollow">http://archives.seul.org/or/talk/Jan-2009/msg00171.html</a></p>

---
_comments:

<a id="comment-560"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-560" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://bsd-tor-exit.dyndns.org">Randy McCallahan (not verified)</a> said:</p>
      <p class="date-time">January 23, 2009</p>
    </div>
    <a href="#comment-560">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-560" class="permalink" rel="bookmark">RE: Tor 0.2.1.11-alpha released</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>@TOR developers</p>
<p>Please wait for publishing of security-related bug information until BSD users are able to upgrade their system. Keep in mind that the BSD ports collection sometimes will be updated with a delay. This means that the time a tor server operator decides to update his system has to be added to the delayed time the new tor version is really available in the BSD ports collection tree.</p>
<p>Tnx<br />
RMcC</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-571"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-571" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 25, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-560" class="permalink" rel="bookmark">RE: Tor 0.2.1.11-alpha released</a> by <a rel="nofollow" href="http://bsd-tor-exit.dyndns.org">Randy McCallahan (not verified)</a></p>
    <a href="#comment-571">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-571" class="permalink" rel="bookmark">We&#039;re waiting.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, we're waiting to disclose the details in order to give users and node operators time to upgrade.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-598"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-598" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">January 31, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-560" class="permalink" rel="bookmark">RE: Tor 0.2.1.11-alpha released</a> by <a rel="nofollow" href="http://bsd-tor-exit.dyndns.org">Randy McCallahan (not verified)</a></p>
    <a href="#comment-598">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-598" class="permalink" rel="bookmark">Re: delaying security advisory so BSD can upgrade</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Can you give us some hint about when you think BSD users will be in better shape? We're currently planning to wait a few weeks, but we weren't planning to wait a few months. Is that too short?</p>
<p>(I need to go bug the Ubuntu people to stop shipping Tor 0.1.2.x and move<br />
to 0.2.0.x, so that's another stumbling block that's holdiing us up.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-562"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-562" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">January 24, 2009</p>
    </div>
    <a href="#comment-562">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-562" class="permalink" rel="bookmark">Warning message in TBB</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Warning message in TBB Controler used oboselete addr-mappings/GETINFO Key; use-address-mappings/instead. Please fix it.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-570"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-570" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 25, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-562" class="permalink" rel="bookmark">Warning message in TBB</a> by <span>PETER (not verified)</span></p>
    <a href="#comment-570">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-570" class="permalink" rel="bookmark">Yes.</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is a problem with Vidalia.  It will be fixed in the next release of Vidalia.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-573"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-573" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">January 25, 2009</p>
    </div>
    <a href="#comment-573">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-573" class="permalink" rel="bookmark">Thanx 4 ur reply phobos!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanx 4 ur reply phobos!</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-579"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-579" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">January 26, 2009</p>
    </div>
    <a href="#comment-579">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-579" class="permalink" rel="bookmark">when will be the next</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>when will be the next release?After releasing it willl be posted here. Please answer.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-582"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-582" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 26, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-579" class="permalink" rel="bookmark">when will be the next</a> by <span>PETER (not verified)</span></p>
    <a href="#comment-582">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-582" class="permalink" rel="bookmark">Unknown</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Vidalia is still very much under active development. I don't know the release schedule at this time.  For now, just ignore that message.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-586"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-586" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">January 26, 2009</p>
    </div>
    <a href="#comment-586">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-586" class="permalink" rel="bookmark">k phobos thanx  4  answering</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>k phobos thanx  4  answering my questions. Looking forward 2  c more.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-599"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-599" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><a rel="nofollow" href="http://bsd-tor-exit.dyndns.org">Randy McCallahan (not verified)</a> said:</p>
      <p class="date-time">February 01, 2009</p>
    </div>
    <a href="#comment-599">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-599" class="permalink" rel="bookmark">Tor BSD update ... ...  *Done*</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Since 29. January the FreeBSD ports of Tor are up to date.</p>
<p><a href="http://www.freshports.org/security/tor-devel/" rel="nofollow">http://www.freshports.org/security/tor-devel/</a><br />
<a href="http://www.freshports.org/security/tor" rel="nofollow">http://www.freshports.org/security/tor</a></p>
<p>Now it's up to the tor operator to update his system.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-610"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-610" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">February 10, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-599" class="permalink" rel="bookmark">Tor BSD update ... ...  *Done*</a> by <a rel="nofollow" href="http://bsd-tor-exit.dyndns.org">Randy McCallahan (not verified)</a></p>
    <a href="#comment-610">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-610" class="permalink" rel="bookmark">Re: Tor BSD update ... ... *Done*</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Great to hear.</p>
<p>But just to keep things exciting, we put out a new stable and development<br />
release today (0.2.0.34 / 0.2.1.12-alpha), with more security fixes. :)</p>
<p>Hopefully these new fixes will last us for a while yet.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
