title: New Releases: Tor 0.3.5.12, 0.4.3.7, and 0.4.4.6
---
pub_date: 2020-11-12
---
author: nickm
---
tags: stable release
---
categories: releases
---
_html_body:

<p>We have a new stable release today. If you build Tor from source, you can download the source code for 0.4.4.6 on the <a href="https://www.torproject.org/download/tor/">download page</a>. Packages should be available within the next several weeks, with a new Tor Browser likely next week.</p>
<p>We've also released 0.3.5.12 (<a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=tor-0.3.5.12">changelog</a>) and 0.4.3.7 (<a href="https://gitweb.torproject.org/tor.git/tree/ChangeLog?h=release-0.4.3&amp;id=be356d80fefa57fbaf58865bde4689fbae30ed09">changelog</a>) today. You can find the source for them at <a href="https://dist.torproject.org">https://dist.torproject.org/</a>, along with older releases.</p>
<p>Tor 0.4.4.6 is the second stable release in the 0.4.4.x series. It backports fixes from later releases, including a fix for TROVE-2020- 005, a security issue that could be used, under certain cases, by an adversary to observe traffic patterns on a limited number of circuits intended for a different relay.</p>
<h2>Changes in version 0.4.4.6 - 2020-11-12</h2>
<ul>
<li>Major bugfixes (security, backport from 0.4.5.1-alpha):
<ul>
<li>When completing a channel, relays now check more thoroughly to make sure that it matches any pending circuits before attaching those circuits. Previously, address correctness and Ed25519 identities were not checked in this case, but only when extending circuits on an existing channel. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40080">40080</a>; bugfix on 0.2.7.2-alpha. Resolves TROVE-2020-005.</li>
</ul>
</li>
<li>Minor features (directory authorities, backport from 0.4.5.1-alpha):
<ul>
<li>Authorities now list a different set of protocols as required and recommended. These lists have been chosen so that only truly recommended and/or required protocols are included, and so that clients using 0.2.9 or later will continue to work (even though they are not supported), whereas only relays running 0.3.5 or later will meet the requirements. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40162">40162</a>.</li>
<li>Make it possible to specify multiple ConsensusParams torrc lines. Now directory authority operators can for example put the main ConsensusParams config in one torrc file and then add to it from a different torrc file. Closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40164">40164</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (subprotocol versions, backport from 0.4.5.1-alpha):
<ul>
<li>Tor no longer allows subprotocol versions larger than 63. Previously version numbers up to UINT32_MAX were allowed, which significantly complicated our code. Implements proposal 318; closes ticket <a href="https://bugs.torproject.org/tpo/core/tor/40133">40133</a>.</li>
</ul>
</li>
<li>Minor features (tests, v2 onion services, backport from 0.4.5.1-alpha):
<ul>
<li>Fix a rendezvous cache unit test that was triggering an underflow on the global rend cache allocation. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40125">40125</a>; bugfix on 0.2.8.1-alpha.</li>
<li>Fix another rendezvous cache unit test that was triggering an underflow on the global rend cache allocation. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40126">40126</a>; bugfix on 0.2.8.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (compilation, backport from 0.4.5.1-alpha):
<ul>
<li>Fix compiler warnings that would occur when building with "--enable-all-bugs-are-fatal" and "--disable-module-relay" at the same time. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40129">40129</a>; bugfix on 0.4.4.1-alpha.</li>
<li>Resolve a compilation warning that could occur in test_connection.c. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40113">40113</a>; bugfix on 0.2.9.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging, backport from 0.4.5.1-alpha):
<ul>
<li>Remove a debug logging statement that uselessly spammed the logs. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40135">40135</a>; bugfix on 0.3.5.0-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (relay configuration, crash, backport from 0.4.5.1-alpha):
<ul>
<li>Avoid a fatal assert() when failing to create a listener connection for an address that was in use. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/40073">40073</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (v2 onion services, backport from 0.4.5.1-alpha):
<ul>
<li>For HSFETCH commands on v2 onion services addresses, check the length of bytes decoded, not the base32 length. Fixes bug <a href="https://bugs.torproject.org/tpo/core/tor/34400">34400</a>; bugfix on 0.4.1.1-alpha. Patch by Neel Chauhan.</li>
</ul>
</li>
</ul>

