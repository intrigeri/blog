title: New Alpha Release: Tor Browser 13.0a3 (Android, Windows, macOS, Linux)
---
pub_date: 2023-08-24
---
author: richard
---
categories:

applications
releases
---
summary: Tor Browser 13.0a3 is now available from the Tor Browser download page and also from our distribution directory.
---
body:
Tor Browser 13.0a3 is now available from the [Tor Browser download page](https://www.torproject.org/download/alpha/) and also from our [distribution directory](https://www.torproject.org/dist/torbrowser/13.0a3/).

This release updates Firefox to 115.2.0esr, including bug fixes, stability improvements and important [security updates](https://www.mozilla.org/en-US/security/advisories/mfsa2023-31/). Android-specific security updates from Firefox 117 are not yet available, but will be part of the next alpha release scheduled in two weeks.

## Major Changes

This is our third alpha release in the 13.0 series which represents a transition from Firefox 102-esr to Firefox 115-esr. This builds on a year's worth of upstream Firefox changes, so alpha-testers should expect to run into issues. If you find any issues, please report them on our [gitlab](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/new) or on the [Tor Project forum](https://forum.torproject.org/c/feedback/tor-browser-alpha-feedback/6).

We are in the middle of our annual esr transition audit, where we review Mozilla's year's worth of work with an eye for privacy and security issues that would negatively affect Tor Browser users. This will be completed before we transition the 13.0 alpha series to stable. At-risk users should remain on the 102-esr based 12.5 stable series which will continue to receive security updates until 13.0 alpha is promoted to stable.

### Build System

We have made the naming scheme for all of our build outputs mutually consistent! This basically means that going forward all our build artifacts will have a name following the form `${ARTIFACT}-${OS}-${ARCH}-${VERSION}.${EXT}`. For example, in 13.0a2 the macOS .dmg pakage was named `TorBrowser-13.0a2-macos_ALL.dmg` whereas in 13.0a3 it is named `tor-browser-macos-13.0a3.dmg`.

If you are a downstream packager or in some other way download Tor Browser artifacts in scripts or automation, you will have a bit more work to do beyond bumping the version number once the 13.0 alpha stabilizes.

## Known Issues

### Windows

Building generated debug headers are not currently reproducible. This only affects debug info and does not affect users. This issue is being tracked [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41995). It will either be fixed before the 13.0 alpha series transitions to stable later this year, or we will disable this developer feature by default to ensure fully matching builds.

### Android

There are various graphical bugs in the bootstrapping and landing pages in Tor Browser for Android including misaligned text and Firefox branding. The Tor Browser onboarding for first-time users is also missing. These issues (among others) are being tracked [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41878), [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41987) and [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41992).

## Full changelog

We would like to thank volunteer contributor cypherpunks1 for their fixes for [tor-browser#40175](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40175), and [tor-browser#41642](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41642). If you would like to contribute, our issue tracker can be found [here](https://gitlab.torproject.org/tpo/applications/tor-browser/-/boards/1243#Platform).

The full changelog since [Tor Browser 13.0a2](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/raw/main/projects/browser/Bundle-Data/Docs-TBB/ChangeLog.txt) is:

- All Platforms
  - Updated Translations
  - [Bug tor-browser#41943](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41943): Lock javascript.options.spectre.disable_for_isolated_content to false
  - [Bug tor-browser#41984](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41984): Rename languageNotification.ftl to base-browser.ftl
  - [Bug tor-browser#42019](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42019): Empty browser's clipboard on browser shutdown
  - [Bug tor-browser#42030](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42030): Rebase the browsers to 115.2.0esr
- Windows + macOS + Linux
  - Updated Firefox to 115.2.0esr
  - [Bug tor-browser#40175](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/40175): Connections in reader mode are not FPI
  - [Bug tor-browser-build#40924](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40924): Customize MOZ_APP_REMOTINGNAME instead of passing --name and --class
  - [Bug tor-browser#41691](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41691): "Firefox Suggest" text appearing in UI
  - [Bug tor-browser#41833](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41833): Reload extensions on new identiy
  - [Bug tor-browser#42022](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42022): Prevent extension search engines from breaking the whole search system
  - [Bug tor-browser#42031](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42031): TBB 13.0a2: TypeError: channel.loadInfo.browsingContext.topChromeWindow.gBrowser is undefined
- Linux
  - [Bug tor-browser-build#40576](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40576): Fontconfig warning: remove 'blank' configuration
- Android
  - Updated GeckoView to 115.2.0esr
  - [Bug tor-browser#42018](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/42018): Rebase Firefox for Android to 115.2.1
- Build System
  - All Platforms
    - [Bug tor-browser-build#40829](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40829): Review and standardize naming scheme for browser installer/package artifacts
    - [Bug tor-browser-build#40921](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40921): staticiforme-prepare-cdn-dist-upload uses hardcoded torbrowser path for .htacess file generation
  - Windows + macOS + Linux
    - [Bug tor-browser-build#40922](https://gitlab.torproject.org/tpo/applications/tor-browser-build/-/issues/40922): Use base-browser.ftl instead of languageNotification.ftl
  - Linux
    - [Bug tor-browser#41967](https://gitlab.torproject.org/tpo/applications/tor-browser/-/issues/41967): Add a Makefile recipe to create multi-lingual dev builds
