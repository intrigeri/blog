title: Pune, India dgplug Meetup: Tor for Journalists and How Developers Can Help
---
pub_date: 2018-04-13
---
author: kushal
---
tags:

meetup
India
---
categories: community
---
summary: The Linux Users’ Group of Durgapur is doing the second monthly meetup in Pune, India. The focus of this meetup is about teaching journalists, activists and common folks how to use Tor and also to learn how to develop tools using the same technologies.
---
_html_body:

<div class="ace-line"> </div>
<div class="ace-line" id="magicdomid321"><a href="https://dgplug.org "><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">The Linux Users’ Group of Durgapur</span></a><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z"> is doing the second monthly meetup in Pune, India. The focus of this meetup is about teaching journalists, activists and common folks how to use Tor and also to learn how to develop tools using the same technologies.</span></div>
<div id="magicdomid6"> </div>
<div class="ace-line" id="magicdomid60"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">The meetup will happen on 18th April, 2018, from 6PM, in the Cafe Coffee Day shop in Magarpatta City, Pune. The evening will start with</span><span class="author-a-z86z5z65zhz87zz85zh3z65zx0k6dg5"> </span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">a brief introduction of the participants </span><span class="author-a-z81z8z122zqfgz78zz72zz71znjgz79zz65zz122zz122z">who would like to introduce themselves</span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">. We have the following topics in mind:</span></div>
<div id="magicdomid8"> </div>
<div class="ace-line" id="magicdomid62"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">* Threat modeling 101</span></div>
<div class="ace-line" id="magicdomid61"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">* Using Tor browser and related tools</span></div>
<div id="magicdomid11"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">* Using SecureDrop to securely leak information to news organizations</span></div>
<div id="magicdomid12"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">* Developing tools for Tor network in Python and Golang</span></div>
<div id="magicdomid13"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">* Free flow discussion</span></div>
<div id="magicdomid14"> </div>
<div class="ace-line" id="magicdomid335"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">Tor is free software and an open network which in turn provides privacy and safety to users. Journalists and activists across the world uses Tor to mitigate surveillance, and it allows them to research in a safe manner. It also helps users to access sites which are blocked by ISPs or by the local government. </span></div>
<div class="ace-line" id="magicdomid240"> </div>
<div class="ace-line" id="magicdomid309"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">In this meetup we will see a live demo of SecureDrop. SecureDrop is an open-source whistleblower submission system maintained by the <a href="https://freedom.press/">Freedom of the Press Foundation</a> which utilizes the Tor network. Media organizations can use it to securely accept documents from and communicate with anonymous sources. For developers, we will also discuss how to start contributing to the project. </span></div>
<div id="magicdomid15"> </div>
<div class="ace-line" id="magicdomid326"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">I</span><span class="author-a-mz67z7z79zz74z2wz70zqgz75zhz69zz79zz66zz83z">f you </span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">a</span><span class="author-a-z81z8z122zqfgz78zz72zz71znjgz79zz65zz122zz122z">re a</span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z"> journalist or not skilled with technology, we can help you answer any</span><span class="author-a-mz67z7z79zz74z2wz70zqgz75zhz69zz79zz66zz83z"> questions </span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">related</span><span class="author-a-mz67z7z79zz74z2wz70zqgz75zhz69zz79zz66zz83z"> Tor or </span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">how</span><span class="author-a-mz67z7z79zz74z2wz70zqgz75zhz69zz79zz66zz83z"> to get started. </span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">We also </span><span class="author-a-mz67z7z79zz74z2wz70zqgz75zhz69zz79zz66zz83z">want </span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">to have a discussion about security practices people follow on </span><span class="author-a-mz67z7z79zz74z2wz70zqgz75zhz69zz79zz66zz83z">the </span><span class="author-a-z81z8z122zqfgz78zz72zz71znjgz79zz65zz122zz122z">i</span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">nternet and </span><span class="author-a-mz67z7z79zz74z2wz70zqgz75zhz69zz79zz66zz83z">in </span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">daily life. We want to speci</span><span class="author-a-z81z8z122zqfgz78zz72zz71znjgz79zz65zz122zz122z">fic</span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">ally invite local journalists, </span><span class="author-a-z81z8z122zqfgz78zz72zz71znjgz79zz65zz122zz122z">as </span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">there will be a group of volunteers to help you out. If you are student or developer, you can also join and find out different ways to contribute upstream.</span></div>
<div id="magicdomid19"> </div>
<div id="magicdomid20"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">Kushal Das, a staff member of the Freedom of the Press Foundation will lead the demo of SecureDrop and discussions around Tor.</span></div>
<div id="magicdomid21"> </div>
<div class="ace-line" id="magicdomid338"><span class="author-a-mz67z7z79zz74z2wz70zqgz75zhz69zz79zz66zz83z">Rather than hav</span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">ing formal talks, </span><span class="author-a-mz67z7z79zz74z2wz70zqgz75zhz69zz79zz66zz83z">our</span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z"> goal is to share knowledge among ourselves and make sure that we all can learn something from each other.</span></div>
<div id="magicdomid23"> </div>
<div class="ace-line" id="magicdomid65"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">There is no admission fee nor RSVP required for this event. Please bring your own mobile </span><span class="author-a-z81z8z122zqfgz78zz72zz71znjgz79zz65zz122zz122z">i</span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">nternet connection</span><span class="author-a-z81z8z122zqfgz78zz72zz71znjgz79zz65zz122zz122z">,</span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z"> as we don't have any other network available at the venue.</span></div>
<div id="magicdomid25"> </div>
<div id="magicdomid26"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z b"><b>Location:</b></span></div>
<div id="magicdomid27"> </div>
<div id="magicdomid28"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z b"><b>18th April, 2018 @ 18:00 IST</b></span></div>
<div id="magicdomid29"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z b"><b>Cafe Coffee Day, 1st Floor Yummy Tummy</b></span></div>
<div id="magicdomid30"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z b"><b>Magarpatta City</b></span></div>
<div id="magicdomid33"> </div>
<div id="magicdomid34"><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z">Join the <a href="http://lists.dgplug.org/listinfo.cgi/users-dgplug.org">dgplug mailing list</a></span><span class="author-a-qz89zvz86z0z89zoz71zz80zz71zz76z90z89ziz75z"> to learn about the upcoming events and discussions.</span></div>

---
_comments:

<a id="comment-274917"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274917" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>henry (not verified)</span> said:</p>
      <p class="date-time">April 16, 2018</p>
    </div>
    <a href="#comment-274917">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274917" class="permalink" rel="bookmark">How do you uninstall tor…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How do you uninstall tor browser</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-274968"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-274968" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span content="How do you uninstall tor browser">How do you uni… (not verified)</span> said:</p>
      <p class="date-time">April 21, 2018</p>
    </div>
    <a href="#comment-274968">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-274968" class="permalink" rel="bookmark">google it. easy</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>google it. easy</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-275771"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275771" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>RT (not verified)</span> said:</p>
      <p class="date-time">June 15, 2018</p>
    </div>
    <a href="#comment-275771">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275771" class="permalink" rel="bookmark">I&#039;m computer illiterate.I…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I'm computer illiterate.I know enough that I don't want big brother or their apparatchiks spying on me and my family.I have a question.</p>
<p>Do I need an additional search engine with the tor browser?  I somehow have DcukDuckgo as my search engine.Is this OK?</p>
<p>Thank you.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-275773"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-275773" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">June 15, 2018</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-275771" class="permalink" rel="bookmark">I&#039;m computer illiterate.I…</a> by <span>RT (not verified)</span></p>
    <a href="#comment-275773">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-275773" class="permalink" rel="bookmark">Yes, that&#039;s okay. The…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Yes, that's okay. The defaults in Tor Browser are chosen so that no one needs to fiddle with internals or adding plugins to get privacy when surfing the web.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
