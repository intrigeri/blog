title: Tor 0.2.1.10-alpha released
---
pub_date: 2009-01-10
---
author: phobos
---
tags:

bug fixes
alpha release
code refactoring
documentation
---
categories: releases
---
_html_body:

<p>Tor 0.2.1.10-alpha fixes two major bugs in bridge relays (one that would<br />
make the bridge relay not so useful if it had DirPort set to 0, and one<br />
that could let an attacker learn a little bit of information about the<br />
bridge's users), and a bug that would cause your Tor relay to ignore a<br />
circuit create request it can't decrypt (rather than reply with an error).<br />
It also fixes a wide variety of other bugs.</p>

<p><a href="https://www.torproject.org/download.html.en" rel="nofollow">https://www.torproject.org/download.html.en</a></p>

<p>Changes in version 0.2.1.10-alpha - 2009-01-06<br />
<strong>Major bugfixes:</strong></p>

<ul>
<li>If the cached networkstatus consensus is more than five days old,<br />
      discard it rather than trying to use it. In theory it could<br />
      be useful because it lists alternate directory mirrors, but in<br />
      practice it just means we spend many minutes trying directory<br />
      mirrors that are long gone from the network. Helps bug 887 a bit;<br />
      bugfix on 0.2.0.x.</li>
<li>Bridge relays that had DirPort set to 0 would stop fetching<br />
      descriptors shortly after startup, and then briefly resume<br />
      after a new bandwidth test and/or after publishing a new bridge<br />
      descriptor. Bridge users that try to bootstrap from them would<br />
      get a recent networkstatus but would get descriptors from up to<br />
      18 hours earlier, meaning most of the descriptors were obsolete<br />
      already. Reported by Tas; bugfix on 0.2.0.13-alpha.</li>
<li>Prevent bridge relays from serving their 'extrainfo' document<br />
      to anybody who asks, now that extrainfo docs include potentially<br />
      sensitive aggregated client geoip summaries. Bugfix on<br />
      0.2.0.13-alpha.</li>
</ul>

<p><strong>Minor features:</strong></p>

<ul>
<li>New controller event "clients_seen" to report a geoip-based summary<br />
      of which countries we've seen clients from recently. Now controllers<br />
      like Vidalia can show bridge operators that they're actually making<br />
      a difference.</li>
<li>Build correctly against versions of OpenSSL 0.9.8 or later built<br />
      without support for deprecated functions.<br />
    - Update to the "December 19 2008" ip-to-country file.</li>
</ul>

<p><strong>Minor bugfixes (on 0.2.0.x):</strong></p>

<ul>
<li>Authorities now vote for the Stable flag for any router whose<br />
      weighted MTBF is at least 5 days, regardless of the mean MTBF.</li>
<li>Do not remove routers as too old if we do not have any consensus<br />
      document. Bugfix on 0.2.0.7-alpha.</li>
<li>Do not accept incomplete ipv4 addresses (like 192.168.0) as valid.<br />
      Spec conformance issue. Bugfix on Tor 0.0.2pre27.</li>
<li>When an exit relay resolves a stream address to a local IP address,<br />
      do not just keep retrying that same exit relay over and<br />
      over. Instead, just close the stream. Addresses bug 872. Bugfix<br />
      on 0.2.0.32. Patch from rovv.</li>
<li>If a hidden service sends us an END cell, do not consider<br />
      retrying the connection; just close it. Patch from rovv.</li>
<li>When we made bridge authorities stop serving bridge descriptors over<br />
      unencrypted links, we also broke DirPort reachability testing for<br />
      bridges. So bridges with a non-zero DirPort were printing spurious<br />
      warns to their logs. Bugfix on 0.2.0.16-alpha. Fixes bug 709.</li>
<li>When a relay gets a create cell it can't decrypt (e.g. because it's<br />
      using the wrong onion key), we were dropping it and letting the<br />
      client time out. Now actually answer with a destroy cell. Fixes<br />
      bug 904. Bugfix on 0.0.2pre8.</li>
<li>Squeeze 2-5% out of client performance (according to oprofile) by<br />
      improving the implementation of some policy-manipulation functions.</li>
</ul>

<p><strong>Minor bugfixes (on 0.2.1.x):</strong></p>

<ul>
<li>Make get_interface_address() function work properly again; stop<br />
      guessing the wrong parts of our address as our address.</li>
<li>Do not cannibalize a circuit if we're out of RELAY_EARLY cells to<br />
      send on that circuit. Otherwise we might violate the proposal-110<br />
      limit. Bugfix on 0.2.1.3-alpha. Partial fix for Bug 878. Diagnosis<br />
      thanks to Karsten.</li>
<li>When we're sending non-EXTEND cells to the first hop in a circuit,<br />
      for example to use an encrypted directory connection, we don't need<br />
      to use RELAY_EARLY cells: the first hop knows what kind of cell<br />
      it is, and nobody else can even see the cell type. Conserving<br />
      RELAY_EARLY cells makes it easier to cannibalize circuits like<br />
      this later.</li>
<li>Stop logging nameserver addresses in reverse order.</li>
<li>If we are retrying a directory download slowly over and over, do<br />
      not automatically give up after the 254th failure. Bugfix on<br />
      0.2.1.9-alpha.</li>
<li>Resume reporting accurate "stream end" reasons to the local control<br />
      port. They were lost in the changes for Proposal 148. Bugfix on<br />
      0.2.1.9-alpha.</li>
</ul>

<p><strong>Deprecated and removed features:</strong></p>

<ul>
<li>The old "tor --version --version" command, which would print out<br />
      the subversion "Id" of most of the source files, is now removed. It<br />
      turned out to be less useful than we'd expected, and harder to<br />
      maintain.</li>
</ul>

<p><strong>Code simplifications and refactoring:</strong></p>

<ul>
<li>Change our header file guard macros to be less likely to conflict<br />
      with system headers. Adam Langley noticed that we were conflicting<br />
      with log.h on Android.</li>
<li>Tool-assisted documentation cleanup. Nearly every function or<br />
      static variable in Tor should have its own documentation now.</li>
</ul>

<p>The original announcement can be found at <a href="http://archives.seul.org/or/talk/Jan-2009/msg00078.html" rel="nofollow">http://archives.seul.org/or/talk/Jan-2009/msg00078.html</a></p>

---
_comments:

<a id="comment-504"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-504" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">January 10, 2009</p>
    </div>
    <a href="#comment-504">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-504" class="permalink" rel="bookmark">No TBB updates  4 this</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>No TBB updates  4 this version.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-530"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-530" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 14, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-504" class="permalink" rel="bookmark">No TBB updates  4 this</a> by <span>PETER (not verified)</span></p>
    <a href="#comment-530">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-530" class="permalink" rel="bookmark">the standard answer</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>...it's coming.  We're working on including FireFox 3, as well as considering a stable and unstable branch for TBB.  We'd rather get it right than get it out quick.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-534"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-534" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">January 15, 2009</p>
    </div>
    <a href="#comment-534">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-534" class="permalink" rel="bookmark">thanx  4  replying 2 my</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>thanx  4  replying 2 my questions. Waiting for the TBB updates.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-550"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-550" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">January 21, 2009</p>
    </div>
    <a href="#comment-550">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-550" class="permalink" rel="bookmark">wat happend 2 TBB updates?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>wat happend 2 TBB updates? getting delayed. Now there is 2.1.11 is released . Still working on it  guys .</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-554"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-554" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 22, 2009</p>
    </div>
    <a href="#comment-554">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-554" class="permalink" rel="bookmark">Coming today</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>TBB 1.1.8 is coming today, should include FF3 and Tor 0.2.1.11-alpha.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-557"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-557" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">January 23, 2009</p>
    </div>
    <a href="#comment-557">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-557" class="permalink" rel="bookmark">Thanx phobos u r great!</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thanx phobos u r great!  Nice work to all of the Tor projet guys who put lots of efforts to make this</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-558"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-558" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>PETER (not verified)</span> said:</p>
      <p class="date-time">January 23, 2009</p>
    </div>
    <a href="#comment-558">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-558" class="permalink" rel="bookmark">Warning message in TBB</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Warning message in TBB Controler used oboselete addr-mappings/GETINFO Key; use-address-mappings/instead. Please fix this  problem</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-566"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-566" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  phobos
  </article>
    <div class="comment-header">
      <p class="comment__submitted">phobos said:</p>
      <p class="date-time">January 25, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-558" class="permalink" rel="bookmark">Warning message in TBB</a> by <span>PETER (not verified)</span></p>
    <a href="#comment-566">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-566" class="permalink" rel="bookmark">It&#039;s a vidalia issue</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It will be fixed in the next release of vidalia.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
