title: Tor Weekly News — March 18th, 2015
---
pub_date: 2015-03-18
---
author: harmony
---
tags: tor weekly news
---
categories: reports
---
_html_body:

<p>Welcome to the eleventh issue in 2015 of Tor Weekly News, the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/tor-news" rel="nofollow">weekly newsletter</a> that covers what’s happening in the Tor community.</p>

<h1>An animated introduction to Tor</h1>

<p>Nima Fatemi <a href="https://blog.torproject.org/blog/releasing-tor-animation" rel="nofollow">announced</a> the release of a short animation offering Internet users a brief and lively introduction to the tracking and surveillance they face online, and the ways in which Tor Browser can protect them. The result of a collaboration between the Tor Project and KAJART Studio, the video is available for download or sharing online, with voiceovers in five languages and subtitles in a further three.</p>

<p>“But we still have work to do”, as Nima explained. “If you have an idea for making better videos and documentation, or if you’re a visual artist and you can help us explain these complex technologies in simple and understandable forms to inexperienced users”, then please see the Tor Project’s <a href="https://www.torproject.org/about/contact" rel="nofollow">website</a> for ways to get in touch; the same goes if you want this animation to be available in your language. See Nima’s post for further details, and be sure to share the video with your friends and contacts!</p>

<h1>Thanks Reddit!</h1>

<p>Following Reddit’s generous <a href="http://www.redditblog.com/2015/02/announcing-winners-of-reddit-donate.html" rel="nofollow">donation</a> to the Tor Project, Tor developers and community members in attendance at the Circumvention Tech Festival in Valencia responded with a surprise <a href="https://media.torproject.org/video/2015-03-11-Tor_Thanks_Reddit.mov" rel="nofollow">video thank-you message</a>. Bonus points if you can identify everyone who took part…</p>

<h1>Miscellaneous news</h1>

<p>Sukhbir Singh <a href="https://blog.torproject.org/blog/torbirdy-014-fifth-beta-and-bug-fix-release" rel="nofollow">announced</a> the release of TorBirdy 0.1.4. This version of the torifying wrapper for Thunderbird fixes a bug that prevented the email client from opening if three or more IMAP accounts are configured. Apart from an <a href="https://lists.torproject.org/pipermail/tor-talk/2015-March/037249.html" rel="nofollow">update to the Whonix gateway</a>, no other features are introduced in this release.</p>

<p>Mike Perry gave <a href="https://lists.torproject.org/pipermail/tor-dev/2015-March/008451.html" rel="nofollow">details</a> of the Tor Browser release cycle, and discussed a possible synchronization with the release schedule for the core Tor software.</p>

<p>Jens Kubieziel published his <a href="https://lists.torproject.org/pipermail/tor-relays/2015-March/006651.html" rel="nofollow">minutes</a> of the Tor relay operators’ meeting in Valencia.</p>

<p>Sukhbir Singh sent out his <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000779.html" rel="nofollow">status report</a> for February, while George Kadianakis submitted the <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000778.html" rel="nofollow">SponsorR report</a>, and Arturo Filastò reported on the activities of the OONI team in <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000780.html" rel="nofollow">January</a> and <a href="https://lists.torproject.org/pipermail/tor-reports/2015-March/000781.html" rel="nofollow">February</a>.</p>

<p>David Fifield published the regular <a href="https://lists.torproject.org/pipermail/tor-dev/2015-March/008427.html" rel="nofollow">summary</a> of costs incurred in February by the infrastructure for meek.</p>

<p>WhonixQubes offered an <a href="https://lists.torproject.org/pipermail/tor-talk/2015-March/037275.html" rel="nofollow">update</a> on recent progress in the Whonix+Qubes project, including architecture improvements, upstream integration, better documentation, and more.</p>

<p>Thanks to <a href="https://lists.torproject.org/pipermail/tor-mirrors/2015-March/000862.html" rel="nofollow">Peter from ftp.yzu.edu.tw</a> for running a mirror of the Tor Project’s website and software!</p>

<p>This issue of Tor Weekly News has been assembled by Harmony and Karsten Loesing.</p>

<p>Want to continue reading TWN? Please help us create this newsletter. We still need more volunteers to watch the Tor community and report important news. Please see the <a href="https://trac.torproject.org/projects/tor/wiki/TorWeeklyNews" rel="nofollow">project page</a>, write down your name and subscribe to the <a href="https://lists.torproject.org/cgi-bin/mailman/listinfo/news-team" rel="nofollow">team mailing list</a> if you want to get involved!</p>

