title: New Release: Tor 0.4.0.4-rc
---
pub_date: 2019-04-11
---
author: nickm
---
tags:

tor releases
release candidate
---
categories: releases
---
summary: Tor 0.4.0.4-rc is the first release candidate in its series; it fixes several bugs from earlier versions, including some that had affected stability, and one that prevented relays from working with NSS.
---
_html_body:

<p>There's a new release candidate available for download. If you build Tor from source, you can download the source code for 0.4.0.4-rc from <a href="https://dist.torproject.org/tor-0.4.0.4-rc.tar.gz">dist.torproject.org</a> (<a href="https://dist.torproject.org/tor-0.4.0.4-rc.tar.gz.asc">pgp signature</a>). Packages should be available over the coming weeks, with a new alpha Tor Browser release by early next week.</p>
<p>Remember, this isn't the stable release yet. You should only run this if you'd like to find and report more bugs than usual.</p>
<p>Tor 0.4.0.4-rc is the first release candidate in its series; it fixes several bugs from earlier versions, including some that had affected stability, and one that prevented relays from working with NSS.</p>
<h2>Changes in version 0.4.0.4-rc - 2019-04-11</h2>
<ul>
<li>Major bugfixes (NSS, relay):
<ul>
<li>When running with NSS, disable TLS 1.2 ciphersuites that use SHA384 for their PRF. Due to an NSS bug, the TLS key exporters for these ciphersuites don't work -- which caused relays to fail to handshake with one another when these ciphersuites were enabled. Fixes bug <a href="https://bugs.torproject.org/29241">29241</a>; bugfix on 0.3.5.1-alpha.</li>
</ul>
</li>
<li>Minor features (bandwidth authority):
<ul>
<li>Make bandwidth authorities ignore relays that are reported in the bandwidth file with the flag "vote=0". This change allows us to report unmeasured relays for diagnostic reasons without including their bandwidth in the bandwidth authorities' vote. Closes ticket <a href="https://bugs.torproject.org/29806">29806</a>.</li>
<li>When a directory authority is using a bandwidth file to obtain the bandwidth values that will be included in the next vote, serve this bandwidth file at /tor/status-vote/next/bandwidth. Closes ticket <a href="https://bugs.torproject.org/21377">21377</a>.</li>
</ul>
</li>
</ul>
<p> </p>
<!--break--><ul>
<li>Minor features (circuit padding):
<ul>
<li>Stop warning about undefined behavior in the probability distribution tests. Float division by zero may technically be undefined behavior in C, but it's well defined in IEEE 754. Partial backport of 29298. Closes ticket <a href="https://bugs.torproject.org/29527">29527</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor features (continuous integration):
<ul>
<li>On Travis Rust builds, cleanup Rust registry and refrain from caching the "target/" directory to speed up builds. Resolves issue <a href="https://bugs.torproject.org/29962">29962</a>.</li>
</ul>
</li>
<li>Minor features (dormant mode):
<ul>
<li>Add a DormantCanceledByStartup option to tell Tor that it should treat a startup event as cancelling any previous dormant state. Integrators should use this option with caution: it should only be used if Tor is being started because of something that the user did, and not if Tor is being automatically started in the background. Closes ticket <a href="https://bugs.torproject.org/29357">29357</a>.</li>
</ul>
</li>
<li>Minor features (geoip):
<ul>
<li>Update geoip and geoip6 to the April 2 2019 Maxmind GeoLite2 Country database. Closes ticket <a href="https://bugs.torproject.org/29992">29992</a>.</li>
</ul>
</li>
<li>Minor features (NSS, diagnostic):
<ul>
<li>Try to log an error from NSS (if there is any) and a more useful description of our situation if we are using NSS and a call to SSL_ExportKeyingMaterial() fails. Diagnostic for ticket <a href="https://bugs.torproject.org/29241">29241</a>.</li>
</ul>
</li>
<li>Minor bugfixes (security):
<ul>
<li>Fix a potential double free bug when reading huge bandwidth files. The issue is not exploitable in the current Tor network because the vulnerable code is only reached when directory authorities read bandwidth files, but bandwidth files come from a trusted source (usually the authorities themselves). Furthermore, the issue is only exploitable in rare (non-POSIX) 32-bit architectures, which are not used by any of the current authorities. Fixes bug <a href="https://bugs.torproject.org/30040">30040</a>; bugfix on 0.3.5.1-alpha. Bug found and fixed by Tobias Stoeckmann.</li>
<li>Verify in more places that we are not about to create a buffer with more than INT_MAX bytes, to avoid possible OOB access in the event of bugs. Fixes bug <a href="https://bugs.torproject.org/30041">30041</a>; bugfix on 0.2.0.16. Found and fixed by Tobias Stoeckmann.</li>
</ul>
</li>
<li>Minor bugfix (continuous integration):
<ul>
<li>Reset coverage state on disk after Travis CI has finished. This should prevent future coverage merge errors from causing the test suite for the "process" subsystem to fail. The process subsystem was introduced in 0.4.0.1-alpha. Fixes bug <a href="https://bugs.torproject.org/29036">29036</a>; bugfix on 0.2.9.15.</li>
<li>Terminate test-stem if it takes more than 9.5 minutes to run. (Travis terminates the job after 10 minutes of no output.) Diagnostic for 29437. Fixes bug <a href="https://bugs.torproject.org/30011">30011</a>; bugfix on 0.3.5.4-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (bootstrap reporting):
<ul>
<li>During bootstrap reporting, correctly distinguish pluggable transports from plain proxies. Fixes bug <a href="https://bugs.torproject.org/28925">28925</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (C correctness):
<ul>
<li>Fix an unlikely memory leak in consensus_diff_apply(). Fixes bug <a href="https://bugs.torproject.org/29824">29824</a>; bugfix on 0.3.1.1-alpha. This is Coverity warning CID 1444119.</li>
</ul>
</li>
<li>Minor bugfixes (circuitpadding testing):
<ul>
<li>Minor tweaks to avoid rare test failures related to timers and monotonic time. Fixes bug <a href="https://bugs.torproject.org/29500">29500</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (directory authorities):
<ul>
<li>Actually include the bandwidth-file-digest line in directory authority votes. Fixes bug <a href="https://bugs.torproject.org/29959">29959</a>; bugfix on 0.4.0.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (logging):
<ul>
<li>On Windows, when errors cause us to reload a consensus from disk, tell the user that we are retrying at log level "notice". Previously we only logged this information at "info", which was confusing because the errors themselves were logged at "warning". Improves previous fix for 28614. Fixes bug <a href="https://bugs.torproject.org/30004">30004</a>; bugfix on 0.4.0.2-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (pluggable transports):
<ul>
<li>Restore old behavior when it comes to discovering the path of a given Pluggable Transport executable file. A change in 0.4.0.1-alpha had broken this behavior on paths containing a space. Fixes bug <a href="https://bugs.torproject.org/29874">29874</a>; bugfix on 0.4.0.1-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (testing):
<ul>
<li>Backport the 0.3.4 src/test/test-network.sh to 0.2.9. We need a recent test-network.sh to use new chutney features in CI. Fixes bug <a href="https://bugs.torproject.org/29703">29703</a>; bugfix on 0.2.9.1-alpha.</li>
<li>Fix a test failure on Windows caused by an unexpected "BUG" warning in our tests for tor_gmtime_r(-1). Fixes bug <a href="https://bugs.torproject.org/29922">29922</a>; bugfix on 0.2.9.3-alpha.</li>
</ul>
</li>
<li>Minor bugfixes (TLS protocol):
<ul>
<li>When classifying a client's selection of TLS ciphers, if the client ciphers are not yet available, do not cache the result. Previously, we had cached the unavailability of the cipher list and never looked again, which in turn led us to assume that the client only supported the ancient V1 link protocol. This, in turn, was causing Stem integration tests to stall in some cases. Fixes bug <a href="https://bugs.torproject.org/30021">30021</a>; bugfix on 0.2.4.8-alpha.</li>
</ul>
</li>
<li>Code simplification and refactoring:
<ul>
<li>Introduce a connection_dir_buf_add() helper function that detects whether compression is in use, and adds a string accordingly. Resolves issue <a href="https://bugs.torproject.org/28816">28816</a>.</li>
<li>Refactor handle_get_next_bandwidth() to use connection_dir_buf_add(). Implements ticket <a href="https://bugs.torproject.org/29897">29897</a>.</li>
</ul>
</li>
<li>Documentation:
<ul>
<li>Clarify that Tor performs stream isolation among *Port listeners by default. Resolves issue <a href="https://bugs.torproject.org/29121">29121</a>.</li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-280817"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280817" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>MrRelentless (not verified)</span> said:</p>
      <p class="date-time">April 12, 2019</p>
    </div>
    <a href="#comment-280817">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280817" class="permalink" rel="bookmark">Absolutely superb news...
…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Absolutely superb news...</p>
<p>Very impressed with the quality and frequency of releases.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-280825"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280825" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Matt (not verified)</span> said:</p>
      <p class="date-time">April 12, 2019</p>
    </div>
    <a href="#comment-280825">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280825" class="permalink" rel="bookmark">I used to have no issues…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I used to have no issues accessing the Settings Page of DuckDuckGo, back when Tor had the Javascript button addon. But sometimes in the past few months, an update has stopped me from being able to access that settings page anymore. How do I fix this? It works on all my other browsers.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280826"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280826" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  gk
  </article>
    <div class="comment-header">
      <p class="comment__submitted">gk said:</p>
      <p class="date-time">April 12, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280825" class="permalink" rel="bookmark">I used to have no issues…</a> by <span>Matt (not verified)</span></p>
    <a href="#comment-280826">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280826" class="permalink" rel="bookmark">Do you have this problem in…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Do you have this problem in a clean, new Tor Browser? Which "Javascript button addon" do you mean? Where can I find the DuckDuckGo settings page?</p>
<p>FWIW: This is a blog post about Tor not Tor Browser and chances are better for some browser person noticing this issue if you at least put it into a Tor Browser related blog post. You could try to contact our support channels as well: <a href="https://2019.www.torproject.org/about/contact.html.en#support" rel="nofollow">https://2019.www.torproject.org/about/contact.html.en#support</a>.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-280833"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280833" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 13, 2019</p>
    </div>
    <a href="#comment-280833">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280833" class="permalink" rel="bookmark">Are you going to add a way…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Are you going to add a way to bypass parental control "pause" features that allow you to connect to the network, but not access the internet? You can go to 192.168.1.1 to request your internet to be unpaused. This system appears to use a firewall. Is it possible to bypass it?</p>
<p>The router that I suffer from blocks unknown devices because I used to spoof my MAC address. I have since started using the MAC addresses of other devices on the network. Maybe Tor could have a way to scan for those and automatically pick one. I have been using arp-scan and nmap for this purpose. I use Ubuntu 18.04, in case you want/need to know.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280850"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280850" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 15, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280833" class="permalink" rel="bookmark">Are you going to add a way…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-280850">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280850" class="permalink" rel="bookmark">&quot;Maybe Tor could have a way…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Maybe Tor could have a way to scan for those and automatically pick one."</p>
<p>With Ubuntu 18.04 you can install tools for or try using TAILS<br />
<a href="https://tails.boum.org" rel="nofollow">https://tails.boum.org</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-280884"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280884" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 18, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280833" class="permalink" rel="bookmark">Are you going to add a way…</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-280884">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280884" class="permalink" rel="bookmark">I think that is outside the…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>I think that is outside the scope of what tor tries to do, and the tools you have been using are already much better for the purpose.  Posing as an existing MAC address on a LAN will cause interference between those two devices and might cause unforeseen problems.  Rather than LAN and MAC configuration, the most that networked client programs try for your purpose is to disguise their traffic as normal web traffic on ports 80 or 443.  As you learned, clients are not always in control of LAN administration.  Moreover, wifi routers have another option called wireless isolation that prevents client devices from scanning other wifi devices on the SSID except for the router.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-280844"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280844" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>cmon (not verified)</span> said:</p>
      <p class="date-time">April 15, 2019</p>
    </div>
    <a href="#comment-280844">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280844" class="permalink" rel="bookmark">Thank you for your project…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Thank you for your project but since today I cannot use the program. I have download the las version of comodo internet security and the cloud antivirus, and the version of tor browser but I cannot use your program. Could you please check it?</p>
<p>Cmon</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-280923"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280923" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 22, 2019</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-280844" class="permalink" rel="bookmark">Thank you for your project…</a> by <span>cmon (not verified)</span></p>
    <a href="#comment-280923">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280923" class="permalink" rel="bookmark">Please comment to a post…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please comment to a post about Tor Browser or contact Tor Project's <a href="https://2019.www.torproject.org/about/contact.html.en#support" rel="nofollow">support channels</a>. This blog post is about the tor binary/exe as distributed in the expert bundle, not the browser bundle.</p>
<p>Read the support site FAQ for <a href="https://support.torproject.org/#tbb" rel="nofollow">Tor Browser</a> and <a href="https://support.torproject.org/#connectingtotor" rel="nofollow">Connecting To Tor</a>.  "Cannot use" is too vague to solve.  Be descriptive.  Are there error messages from Tor Browser or from your security programs?  What is your platform/OS?  Which version of Tor Browser?  Alpha version or not?  Did you modify bridges, proxy, or port options before connecting?  Did you modify Tor Browser after connecting?</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-280865"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280865" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>nowitz (not verified)</span> said:</p>
      <p class="date-time">April 17, 2019</p>
    </div>
    <a href="#comment-280865">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280865" class="permalink" rel="bookmark">fuck not working , android 4…</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>fuck not working , android 4 &amp; 5</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-280868"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-280868" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Ditomagico555 (not verified)</span> said:</p>
      <p class="date-time">April 17, 2019</p>
    </div>
    <a href="#comment-280868">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-280868" class="permalink" rel="bookmark">Solo incontri nient altro</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Solo incontri nient altro</p>
</div>
  </div>
</article>
<!-- Comment END -->
