title: BitTorrent support for Thandy
---
pub_date: 2009-09-10
---
author: Sebastian
---
tags:

gsoc
google
Google Summer of Code
thandy
BitTorrent
---
categories: internships
---
_html_body:

<p>As a returning <a href="http://socghop.appspot.com/org/home/google/gsoc2009/eff" rel="nofollow">Google Summer of Code</a> student for the second year in a row, I was thrilled to hear that I had been accepted again.</p>

<p>My task was to add <a href="http://www.bittorrent.com/" rel="nofollow">BitTorrent</a> support to <a href="https://git.torproject.org/checkout/thandy/master/" rel="nofollow">Thandy</a>, the secure automated updater <a href="http://google-opensource.blogspot.com/2009/03/thandy-secure-update-for-tor.html" rel="nofollow">developed</a> by the Tor project, along with setting up and testing the necessary infrastructure. The goal is to better mitigate load spikes following the release of new software versions and allowing volunteers to easily help users to fetch Tor.</p>

<p>Of course, working wasn't without its own challenges. My mentor and I live in different parts of the world, and real-time communication was often difficult to achieve due to timezone differences. Another big challenge was staying motivated during the first month, where university demanded a lot of attention. The situation improved substantially after the mid-term evaluation, when school-related work became less time consuming and I could focus on SoC entirely. During the whole time, I communicated with some of Tor's other SoC students, helping, encouraging and sharing some great laughs with each other. I feel that the community aspect of GSoC worked out much better this year.</p>

<p>During the summer, it became apparent that following through with the original plan of using the <a href="http://www.rasterbar.com/products/libtorrent/index.html" rel="nofollow">C++ libtorrent implementation by rasterbar</a> would be difficult, due to various platform-independence issues and a huge resulting binary. Especially the last point made it clear that we wouldn't be able to use that for our netinstaller, one of the most important reasons to develop Thandy. Eventually, I settled on the original BitTorrent implementation, patched by Debian to work around some issues with newer versions of Python.</p>

<p>It was great to see that this year, all my code has been merged into the Thandy source. I think my experience from last year saved me from making the same mistakes again, by focusing on small parts that work instead of re-designing major aspects of the software.</p>

<p>Overall, I'm very happy with how my Summer of Code turned out to be, and I hope to participate again (either as student or even mentor)!</p>

---
_comments:

<a id="comment-2471"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2471" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">September 11, 2009</p>
    </div>
    <a href="#comment-2471">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2471" class="permalink" rel="bookmark">Tor/BT</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Would the Bittorrent protocol itself be able to profit from your efforts in any way ?<br />
(You've probably been asked this before, what with BitBlinder et al.)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-2474"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2474" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  Sebastian
  </article>
    <div class="comment-header">
      <p class="comment__submitted">Sebastian said:</p>
      <p class="date-time">September 11, 2009</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-2471" class="permalink" rel="bookmark">Tor/BT</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-2474">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2474" class="permalink" rel="bookmark">Not sure what you&#039;re</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Not sure what you're referring to. If you mean bittorrent over Tor, then no, that won't help, unfortunately.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-2973"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-2973" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Sleek (not verified)</span> said:</p>
      <p class="date-time">October 29, 2009</p>
    </div>
    <a href="#comment-2973">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-2973" class="permalink" rel="bookmark">Please do you know how I</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Please do you know how I could configure bittorrent to work with tor? thanks in advance</p>
</div>
  </div>
</article>
<!-- Comment END -->
