title: Tor 0.2.8.11 is released, with small portability fixes
---
pub_date: 2016-12-08
---
author: nickm
---
tags:

tor
stable
release
---
categories:

network
releases
---
_html_body:

<p>There's a new stable release of Tor!<br />
Tor 0.2.8.11 backports fixes for additional portability issues that could prevent Tor from building correctly on OSX Sierra, or with OpenSSL 1.1. Affected users should upgrade; others can safely stay with 0.2.8.10.<br />
You can download the source from the usual place on the website. Packages should be available over the next several days, including a TorBrowser release around December 14. Remember to check the signatures!<br />
Below are the changes since 0.2.8.10.</p>

<h2>Changes in version 0.2.8.11 - 2016-12-08</h2>

<ul>
<li>Minor bugfixes (portability):
<ul>
<li>Avoid compilation errors when building on OSX Sierra. Sierra began to support the getentropy() and clock_gettime() APIs, but created a few problems in doing so. Tor 0.2.9 has a more thorough set of workarounds; in 0.2.8, we are just using the /dev/urandom and mach monotonic time interfaces. Fixes bug <a href="https://bugs.torproject.org/20865" rel="nofollow">20865</a>. Bugfix on 0.2.8.1-alpha.
  </li>
</ul>
</li>
<li>Minor bugfixes (portability, backport from 0.2.9.5-alpha):
<ul>
<li>Fix compilation with OpenSSL 1.1 and less commonly-used CPU architectures. Closes ticket <a href="https://bugs.torproject.org/20588" rel="nofollow">20588</a>.
  </li>
</ul>
</li>
</ul>

---
_comments:

<a id="comment-224688"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224688" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">December 10, 2016</p>
    </div>
    <a href="#comment-224688">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224688" class="permalink" rel="bookmark">Just (Dec. 9/16) updated</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Just (Dec. 9/16) updated onion browser on my iPhone, now the app won't even open.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-224750"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-224750" class="contextual-region comment js-comment" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  arma
  </article>
    <div class="comment-header">
      <p class="comment__submitted">arma said:</p>
      <p class="date-time">December 11, 2016</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-224688" class="permalink" rel="bookmark">Just (Dec. 9/16) updated</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-224750">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-224750" class="permalink" rel="bookmark">You should be sure to tell</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>You should be sure to tell the onion browser person about this, since the comment on this blog post is not the way to do that.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div>
