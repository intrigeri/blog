title: Web Developers and Firefox Hackers: Help us with Firefox 4
---
pub_date: 2011-03-25
---
author: mikeperry
---
tags:

tor
torbutton
firefox
---
categories:

applications
network
---
_html_body:

<p>We need some web-savvy people to help us audit the <a href="https://www.torproject.org/torbutton/" rel="nofollow">Torbutton alpha series</a> for Firefox 4.  I've performed a preliminary audit, and Torbutton 1.3.2-alpha should be safe from major issues, but a lot more testing is needed. In particular, we need people to test the <a href="https://developer.mozilla.org/en/Firefox_4_for_developers" rel="nofollow">new Firefox 4 features</a>.</p>

<p>The <a href="https://gitweb.torproject.org/torbutton.git/blob_plain/HEAD:/website/design/FF40_AUDIT" rel="nofollow">notes from my preliminary audit</a> are available in the Torbutton git repository, but note that I have not tested everything that struck me as potentially troublesome, and there may be other things I missed too.</p>

<p>As a reminder, the types of things we are looking for are things that violate the <a href="https://www.torproject.org/torbutton/en/design/#requirements" rel="nofollow">Torbutton Security Requirements</a>, which may include new ways to bypass proxy settings, to fingerprint users, or to use novel identifiers to correlate Tor and Non-Tor activity.</p>

<p>In addition, we may have some funding to address <a href="https://www.torproject.org/torbutton/en/design/#FirefoxBugs" rel="nofollow">outstanding Torbutton-related bugs</a> in Firefox. If you know C++ and/or Firefox internals, we should be able to pay you for your time to address these issues and shepherd the relevant patches through Mozilla's review process.</p>

<p>If you find issues, or if you are interested in working on fixing these bugs, please contact us at tor-assistants at torproject dot org. Torbutton bugs that you find can be added to the growing pile at the <a href="https://trac.torproject.org/projects/tor/report/14" rel="nofollow">Torbutton Bug Tracker</a>.</p>

<p>The sooner we get these issues taken care of, the sooner we can confidently release a stable Torbutton for Firefox 4.</p>

---
_comments:

<a id="comment-9722"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9722" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 25, 2011</p>
    </div>
    <a href="#comment-9722">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9722" class="permalink" rel="bookmark">How about a Tor bug bounty?</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>How about a Tor bug bounty? :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-9755"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9755" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mikeperry
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mikeperry said:</p>
      <p class="date-time">March 26, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-9722" class="permalink" rel="bookmark">How about a Tor bug bounty?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-9755">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9755" class="permalink" rel="bookmark">This is exactly what we are</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>This is exactly what we are planning for the Firefox bugs. We also can pay for certain Tor tasks as well, but usually we want those done on a larger amount of work/timescale than per-bug basis, mostly because it is painful to assign a public dollar amount to *every* issue and task we want completed.</p>
<p>Do you think we should assign explicit, public dollar amounts to the Firefox bugs? The problem I see with this is that the Mozilla code review process is an especially painful one. You can literally end up shepherding a patch for years (I'm not kidding: see <a href="https://bugzilla.mozilla.org/show_bug.cgi?id=280661" rel="nofollow">Bug 280661</a>).</p>
<p>I think this means we probably should assign two sets of hourly rates, one before code review and one after, perhaps with two ceilings on these amounts. The rate before code review/submission would be higher, to encourage better work up front. Or perhaps this not right either.</p>
<p>But, I think if we do not figure out a way to both allow people to bill before the patch is actually merged (perhaps once we merge it into Tor Browser Bundle, for example), and also to get compensated for dealing with Mozilla bureaucracy, we're going to end up with only partially written patches that never get finished, and/or we're not going to get any responses from people who have dealt with Mozilla before.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9757"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9757" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mikeperry
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mikeperry said:</p>
      <p class="date-time">March 26, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-9722" class="permalink" rel="bookmark">How about a Tor bug bounty?</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-9757">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9757" class="permalink" rel="bookmark">Oh, I just realized you want</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Oh, I just realized you want to be paid for <i>exploits</i> of Tor ;).</p>
<p>Hrmm, we'll need to think about this. Not all of Torbutton's Security Requirements are of equal worth/importance. #1 is proxy bypass, #2 is correlation of tor to non-tor. Then, much farther down, are things like fingerprinting and anonymity set reduction.</p>
<p>If you find a bug against #1 (and maybe also #2, depending on the strength of correlation), we could possibly work something out :)</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-9739"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9739" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 26, 2011</p>
    </div>
    <a href="#comment-9739">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9739" class="permalink" rel="bookmark">Peace be with you.
Just to</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Peace be with you.<br />
Just to let you know that i/we do really appreciate the paramount drive to keep us up our and/or  every of individual dignity and respect to privacy which we consider part of our life sanctity.<br />
Keep it on!<br />
God Bless.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9814"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9814" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">March 31, 2011</p>
    </div>
    <a href="#comment-9814">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9814" class="permalink" rel="bookmark">It would help if you would</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>It would help if you would Update the outstanding Torbutton-related bugs page:</p>
<p>=============</p>
<p>6.1. Bugs impacting security</p>
<p>2. Bug 280661 - RESOLVED FIXED </p>
<p>3. Bug 418986 - Bounty by someone else</p>
<p>"JonDonym 2010-09-10 11:40:46 PDT<br />
We would pay 300 Euro for someone fixing this bug and putting it in the<br />
official Mozilla code. And another 300 Euro for fixing it with CSS media<br />
queries. Just contact me for further questions and discussions about the<br />
implementation."</p>
<p>=============</p>
<p>6.2. Bugs blocking functionality</p>
<p>1. Bug 445696 - RESOLVED DUPLICATE of bug 484832<br />
2. Bug 290456 - RESOLVED FIXED </p>
<p>============<br />
6.3. Low Priority Bugs</p>
<p>1. Bug 435151 - RESOLVED FIXED</p>
</div>
  </div>
</article>
<!-- Comment END -->
<div class="indented"><a id="comment-9833"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9833" class="contextual-region comment js-comment by-node-author" id="comment-">
  <footer class="comment__meta">
    <article class="contextual-region">
  mikeperry
  </article>
    <div class="comment-header">
      <p class="comment__submitted">mikeperry said:</p>
      <p class="date-time">April 04, 2011</p>
    </div>
              <p class="parent visually-hidden">In reply to <a href="#comment-9814" class="permalink" rel="bookmark">It would help if you would</a> by <span>Anonymous (not verified)</span></p>
    <a href="#comment-9833">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9833" class="permalink" rel="bookmark">Ok, I&#039;ve updated the design</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Ok, I've updated the design doc bug list to take care of this. The list also needs another update to cover TLS issues, and new fingerprinting issues introduced in FF4.</p>
</div>
  </div>
</article>
<!-- Comment END -->
</div><a id="comment-9834"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9834" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 04, 2011</p>
    </div>
    <a href="#comment-9834">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9834" class="permalink" rel="bookmark">&quot;Oh, I just realized you</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>"Oh, I just realized you want to be paid for exploits of Tor ;)."</p>
<p><a href="http://techcrunch.com/2010/03/19/google-chrome-1337/" rel="nofollow">http://techcrunch.com/2010/03/19/google-chrome-1337/</a></p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9839"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9839" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 05, 2011</p>
    </div>
    <a href="#comment-9839">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9839" class="permalink" rel="bookmark">Firefox 4.0 + Torbutton</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p><strong>Firefox 4.0 + Torbutton 1.3.2-alpha leaks:</strong><br />
1. Browser sends Accept-Encoding="gzip,<em>SPACE</em>deflate" instead of just "gzip,deflate" which is pretty unique (test at panopticlick.eff.org).<br />
2. User-Agent says Firefox/3.6.3, but browser sends the DNT header which, afaik, only appeared in Firefox 4.</p>
<p>Temporary solution is to respectively set and filter headers by hand with ModifyHeaders. Also, Noscript sends DNT on its own and bypasses ModifyHeaders. Can be turned off in about:config noscript.doNotTrack.enabled. One can test DNT presence and other headers with TamperData.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9926"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9926" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 25, 2011</p>
    </div>
    <a href="#comment-9926">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9926" class="permalink" rel="bookmark">Dear Sir,
 I have no</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Dear Sir,<br />
 I have no technical knowledge and unfortunately many of my fellow activists lack tech savvy, we can't all be programmers. But as you may have noticed the Arab spring is chugging on the thing is the easy countries are done. the very very hard ones are left AKA Saudi Arabia. there are about 40,000 political prisoners in Saudi. Frankly I don't want to add to that number until we are ready to revolt. So we need tor. We need tor-button for firefox4. We need it in a few months. how can we help you get there? how much money do you need for that? I might be able to entice a few to chip in if you give me a target.<br />
thank you very much you have already saved lives in Tunisia and Syria hopefully we will owe some of our necks to you guys too.</p>
</div>
  </div>
</article>
<!-- Comment END -->
<a id="comment-9944"></a>
<div class="comment-meta-filler">
<!-- Comment -->
<article id="comment-9944" class="contextual-region comment js-comment by-anonymous" id="comment-">
  <footer class="comment__meta">
    <div class="comment-header">
      <p class="comment__submitted"><span>Anonymous (not verified)</span> said:</p>
      <p class="date-time">April 28, 2011</p>
    </div>
    <a href="#comment-9944">Permalink</a>
  </footer>
  <div class="content">
      <h3><a href="#comment-9944" class="permalink" rel="bookmark">Torbutton 1.3.2 is</a></h3>
      <div></div>
            <div class="field field--name-comment-body field--type-text-long field--label-hidden field--item"><p>Torbutton 1.3.2 is incompatible with the 4.0.1 update to Firefox.</p>
</div>
  </div>
</article>
<!-- Comment END -->
