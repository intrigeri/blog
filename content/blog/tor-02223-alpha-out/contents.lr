title: Tor 0.2.2.23-alpha is out
---
pub_date: 2011-03-10
---
author: erinn
---
tags:

tor
bug fixes
alpha release
directory authority
---
categories:

network
releases
---
_html_body:

<p>Tor 0.2.2.23-alpha lets relays record their bandwidth history so when<br />
  they restart they don't lose their bandwidth capacity estimate. This<br />
  release also fixes a diverse set of user-facing bugs, ranging from<br />
  relays overrunning their rate limiting to clients falsely warning about<br />
  clock skew to bridge descriptor leaks by our bridge directory authority.</p>

<p><a href="https://torproject.org/download/download" rel="nofollow">https://torproject.org/download/download</a></p>

<p><strong>Major bugfixes:</strong></p>

<ul>
<li>Stop sending a CLOCK_SKEW controller status event whenever<br />
      we fetch directory information from a relay that has a wrong clock.<br />
      Instead, only inform the controller when it's a trusted authority<br />
      that claims our clock is wrong. Bugfix on 0.1.2.6-alpha; fixes<br />
      the rest of bug 1074.</li>
<li>Fix an assert in parsing router descriptors containing IPv6<br />
      addresses. This one took down the directory authorities when<br />
      somebody tried some experimental code. Bugfix on 0.2.1.3-alpha.</li>
<li>Make the bridge directory authority refuse to answer directory<br />
      requests for "all" descriptors. It used to include bridge<br />
      descriptors in its answer, which was a major information leak.<br />
      Found by "piebeer". Bugfix on 0.2.0.3-alpha.</li>
<li>If relays set RelayBandwidthBurst but not RelayBandwidthRate,<br />
      Tor would ignore their RelayBandwidthBurst setting,<br />
      potentially using more bandwidth than expected. Bugfix on<br />
      0.2.0.1-alpha. Reported by Paul Wouters. Fixes bug 2470.</li>
<li>Ignore and warn if the user mistakenly sets "PublishServerDescriptor<br />
      hidserv" in her torrc. The 'hidserv' argument never controlled<br />
      publication of hidden service descriptors. Bugfix on 0.2.0.1-alpha.</li>
</ul>

<p>  <strong>Major features:</strong></p>

<ul>
<li>Relays now save observed peak bandwidth throughput rates to their<br />
      state file (along with total usage, which was already saved)<br />
      so that they can determine their correct estimated bandwidth on<br />
      restart. Resolves bug 1863, where Tor relays would reset their<br />
      estimated bandwidth to 0 after restarting.</li>
<li>Directory authorities now take changes in router IP address and<br />
      ORPort into account when determining router stability. Previously,<br />
      if a router changed its IP or ORPort, the authorities would not<br />
      treat it as having any downtime for the purposes of stability<br />
      calculation, whereas clients would experience downtime since the<br />
      change could take a while to propagate to them. Resolves issue 1035.</li>
<li>Enable Address Space Layout Randomization (ASLR) and Data Execution<br />
      Prevention (DEP) by default on Windows to make it harder for<br />
      attackers to exploit vulnerabilities. Patch from John Brooks.</li>
</ul>

<p>  <strong>Minor bugfixes (on 0.2.1.x and earlier):</strong></p>

<ul>
<li>Fix a rare crash bug that could occur when a client was configured<br />
      with a large number of bridges. Fixes bug 2629; bugfix on<br />
      0.2.1.2-alpha. Bugfix by trac user "shitlei".</li>
<li>Avoid a double mark-for-free warning when failing to attach a<br />
      transparent proxy connection. Bugfix on 0.1.2.1-alpha. Fixes<br />
      bug 2279.</li>
<li>Correctly detect failure to allocate an OpenSSL BIO. Fixes bug 2378;<br />
      found by "cypherpunks". This bug was introduced before the first<br />
      Tor release, in svn commit r110.</li>
<li>Country codes aren't supported in EntryNodes until 0.2.3.x, so<br />
      don't mention them in the manpage. Fixes bug 2450; issue<br />
      spotted by keb and G-Lo.</li>
<li>Fix a bug in bandwidth history state parsing that could have been<br />
      triggered if a future version of Tor ever changed the timing<br />
      granularity at which bandwidth history is measured. Bugfix on<br />
      Tor 0.1.1.11-alpha.</li>
<li>When a relay decides that its DNS is too broken for it to serve<br />
      as an exit server, it advertised itself as a non-exit, but<br />
      continued to act as an exit. This could create accidental<br />
      partitioning opportunities for users. Instead, if a relay is<br />
      going to advertise reject *:* as its exit policy, it should<br />
      really act with exit policy "reject *:*". Fixes bug 2366.<br />
      Bugfix on Tor 0.1.2.5-alpha. Bugfix by user "postman" on trac.</li>
<li>In the special case where you configure a public exit relay as your<br />
      bridge, Tor would be willing to use that exit relay as the last<br />
      hop in your circuit as well. Now we fail that circuit instead.<br />
      Bugfix on 0.2.0.12-alpha. Fixes bug 2403. Reported by "piebeer".</li>
<li>Fix a bug with our locking implementation on Windows that couldn't<br />
      correctly detect when a file was already locked. Fixes bug 2504,<br />
      bugfix on 0.2.1.6-alpha.</li>
<li>Fix IPv6-related connect() failures on some platforms (BSD, OS X).<br />
      Bugfix on 0.2.0.3-alpha; fixes first part of bug 2660. Patch by<br />
      "piebeer".</li>
<li>Set target port in get_interface_address6() correctly. Bugfix<br />
      on 0.1.1.4-alpha and 0.2.0.3-alpha; fixes second part of bug 2660.</li>
<li>Directory authorities are now more robust to hops back in time<br />
      when calculating router stability. Previously, if a run of uptime<br />
      or downtime appeared to be negative, the calculation could give<br />
      incorrect results. Bugfix on 0.2.0.6-alpha; noticed when fixing<br />
      bug 1035.</li>
<li>Fix an assert that got triggered when using the TestingTorNetwork<br />
      configuration option and then issuing a GETINFO config-text control<br />
      command. Fixes bug 2250; bugfix on 0.2.1.2-alpha.</li>
</ul>

<p>  <strong>Minor bugfixes (on 0.2.2.x):</strong></p>

<ul>
<li>Clients should not weight BadExit nodes as Exits in their node<br />
      selection. Similarly, directory authorities should not count BadExit<br />
      bandwidth as Exit bandwidth when computing bandwidth-weights.<br />
      Bugfix on 0.2.2.10-alpha; fixes bug 2203.</li>
<li>Correctly clear our dir_read/dir_write history when there is an<br />
      error parsing any bw history value from the state file. Bugfix on<br />
      Tor 0.2.2.15-alpha.</li>
<li>Resolve a bug in verifying signatures of directory objects<br />
      with digests longer than SHA1. Bugfix on 0.2.2.20-alpha.<br />
      Fixes bug 2409. Found by "piebeer".</li>
<li>Bridge authorities no longer crash on SIGHUP when they try to<br />
      publish their relay descriptor to themselves. Fixes bug 2572. Bugfix<br />
      on 0.2.2.22-alpha.</li>
</ul>

<p>  <strong>Minor features:</strong></p>

<ul>
<li>Log less aggressively about circuit timeout changes, and improve<br />
      some other circuit timeout messages. Resolves bug 2004.</li>
<li>Log a little more clearly about the times at which we're no longer<br />
      accepting new connections. Resolves bug 2181.</li>
<li>Reject attempts at the client side to open connections to private<br />
      IP addresses (like 127.0.0.1, 10.0.0.1, and so on) with<br />
      a randomly chosen exit node. Attempts to do so are always<br />
      ill-defined, generally prevented by exit policies, and usually<br />
      in error. This will also help to detect loops in transparent<br />
      proxy configurations. You can disable this feature by setting<br />
      "ClientRejectInternalAddresses 0" in your torrc.</li>
<li>Always treat failure to allocate an RSA key as an unrecoverable<br />
      allocation error.</li>
<li>Update to the March 1 2011 Maxmind GeoLite Country database.</li>
</ul>

<p>  <strong>Minor features (log subsystem):</strong></p>

<ul>
<li>Add documentation for configuring logging at different severities in<br />
      different log domains. We've had this feature since 0.2.1.1-alpha,<br />
      but for some reason it never made it into the manpage. Fixes<br />
      bug 2215.</li>
<li>Make it simpler to specify "All log domains except for A and B".<br />
      Previously you needed to say "[*,~A,~B]". Now you can just say<br />
      "[~A,~B]".</li>
<li>Add a "LogMessageDomains 1" option to include the domains of log<br />
      messages along with the messages. Without this, there's no way<br />
      to use log domains without reading the source or doing a lot<br />
      of guessing.</li>
</ul>

<p>  <strong>Packaging changes:</strong></p>

<ul>
<li>Stop shipping the Tor specs files and development proposal documents<br />
      in the tarball. They are now in a separate git repository at<br />
      git://git.torproject.org/torspec.git</li>
</ul>

